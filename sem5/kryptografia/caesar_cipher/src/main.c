#include <stdio.h>
#include <stdlib.h>

void caesar_upper_case(char cipher[], int shift) {

    for(; *cipher; cipher++){
        if(*cipher >= 'A' && *cipher <= 'Z') {
            *cipher = (((*cipher - 'A' + shift) % 26 + 26) % 26) + 'A';

            //*cipher = ((*cipher - 'A' + shift) % ('Z'-'A' + 1)) + 'A';
        }
    }
}

void caesar_general(char *cipher,int shift) {
    for(; *cipher; cipher++){
        if(*cipher >= 'A' && *cipher <= 'Z') {
            *cipher = ((*cipher - 'A' + shift) % ('Z'-'A' + 1)) + 'A';
        }else if(*cipher >= 'a' && *cipher <= 'z') {
            *cipher = ((*cipher - 'a' + shift) % ('z'-'a' + 1)) + 'a';
        }else if(*cipher >= '0' && *cipher <= '9') {
            *cipher = ((*cipher - '0' + shift) % ('9'-'0' + 1)) + '0';
        }
    }
}

int main (int argc, const char* argv[]){

#if 0
    for(int i = 127; i >= -127; i--){
        char tekst[] = "WP NSTQQCP OP GTRPYPCE PDE FY DJDEPXP OP NSTQQCPXPYE AZWJLWASLMPETBFP";
        caesar_upper_case(tekst, i);  
        printf("%d %s\n\n", i,tekst);
    }
#endif 

#if 0
    for(int i = 127; i >= -127; i--){
        char tekst[] = "wP nstqqcp OP gtrpypce PDE FY DJDEPXP OP NSTQQCPXPYE AZWJLWASLMPETBFP CFOJ 657";
        caesar_general(tekst,i);  
        printf("%d %s\n\n", i,tekst);
    }
#endif 

#if 1
    if(argc < 2){
        return 1;
    }

    char buf[1024];
    const int shift = strtol(argv[1],NULL,10);

    int read; 
    while ((read = fread(buf, sizeof(char), sizeof(buf) - 1, stdin))) {
        buf[read] = '\0';

        // encrypt print to stdout
        caesar_general(buf, shift);  
        printf("%d %s\n\n", shift ,buf);
    }
#endif
    return 0;
}

