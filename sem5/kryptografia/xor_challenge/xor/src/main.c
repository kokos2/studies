
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>

#define BUFF_SIZE 128
#define MAX_KEY_LENGTH 16
#define NUM_THREADS 6
#define VALIDATION_LENGTH 8  // Only validate the first 8 bytes initially

// Structure to pass arguments to each thread
typedef struct {
    const char *cipher;
    int cipher_length;
    int start_range;
    int end_range;
} ThreadArgs;

// Helper function to check if a character is alphabetic, a digit, or one of " ", ".", ","
int is_valid_decrypted_char(char ch) {
    return ((ch >= 'A' && ch <= 'Z') || 
            (ch >= 'a' && ch <= 'z') || 
            //(ch >= '0' && ch <= '9') || 
            ch == ' ' || 
            ch == ',' || 
            ch == '.');
}

// Optimized decrypt function with full decryption and early exit if invalid character found
int decrypt_with_key_and_check(const char *cipher, const char *key, int cipher_length, int key_length, char *result) {
    for (int i = 0; i < cipher_length; i++) {
        result[i] = cipher[i] ^ key[i % key_length];
        
        // Early exit if we encounter a non-valid character at any point
        if (!is_valid_decrypted_char(result[i])) {
            return 0;  // Return 0 to indicate failure (non-valid character found)
        }
    }
    result[cipher_length] = '\0';  // Null-terminate the result

    return 1;  // Return 1 to indicate success (all characters are valid)
}

// Recursive function to generate all keys of a specific length
void generate_keys_and_decrypt(const char *cipher, int cipher_length, char *key, int position, int key_length) {
    if (position == key_length) {
        // When the key is fully formed, attempt decryption
        char result[BUFF_SIZE + 1];
        if (decrypt_with_key_and_check(cipher, key, cipher_length, key_length, result)) {
            printf("Key (length %d): ", key_length);
            for (int i = 0; i < key_length; i++) {
                printf("%02X", (unsigned char)key[i]);
            }
            printf(" -> Decrypted: %s\n", result);
        }
        return;
    }

    // Try all possible values for the current position in the key
    for (int i = 0; i <= 0xFF; i++) {
        key[position] = (char)i;
        generate_keys_and_decrypt(cipher, cipher_length, key, position + 1, key_length);
    }
}

// Thread function to handle a portion of the keyspace
void *brute_force_decrypt_thread(void *args) {
    ThreadArgs *thread_args = (ThreadArgs *)args;
    const char *cipher = thread_args->cipher;
    int cipher_length = thread_args->cipher_length;
    int start_range = thread_args->start_range;
    int end_range = thread_args->end_range;

    char key[MAX_KEY_LENGTH + 1] = {0};  // Buffer for the key, +1 for null terminator

    // Try all key lengths from 1 to MAX_KEY_LENGTH
    for (int key_length = 1; key_length <= MAX_KEY_LENGTH; key_length++) {
        // Initialize the first position of the key with the range specific to this thread
        for (int i = start_range; i <= end_range; i++) {
            key[0] = (char)i;
            generate_keys_and_decrypt(cipher, cipher_length, key, 1, key_length);
        }
    }

    pthread_exit(NULL);
}

int main() {
    const char *cipher = "FA600BA9";

    // Truncate cipher to 128 bytes for testing purposes
    char sub_cipher[BUFF_SIZE + 1] = {0};
    strncpy(sub_cipher, cipher, BUFF_SIZE);

    // Define thread arguments for each thread
    pthread_t threads[NUM_THREADS];
    ThreadArgs thread_args[NUM_THREADS];
    int range_size = 256 / NUM_THREADS;  // Divide the range equally among threads

    for (int i = 0; i < NUM_THREADS; i++) {
        thread_args[i].cipher = sub_cipher;
        thread_args[i].cipher_length = BUFF_SIZE;
        thread_args[i].start_range = i * range_size;
        thread_args[i].end_range = (i + 1) * range_size - 1;

        // Create each thread
        if (pthread_create(&threads[i], NULL, brute_force_decrypt_thread, &thread_args[i]) != 0) {
            perror("Failed to create thread");
            return 1;
        }
    }

    // Wait for all threads to finish
    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    return 0;
}


#if 0
const char *hex_to_bin_char(char hex) {
    switch (hex) {
        case '0': { return "0000"; }
        case '1': { return "0001"; }
        case '2': { return "0010"; }
        case '3': { return "0011"; }
        case '4': { return "0100"; }
        case '5': { return "0101"; }
        case '6': { return "0110"; }
        case '7': { return "0111"; }
        case '8': { return "1000"; }
        case '9': { return "1001"; }
        // might need to separate it
        case 'A': case 'a': 
            { return "1010"; }
        case 'B': case 'b': 
            { return "1011"; }
        case 'C': case 'c': 
            { return "1100"; }
        case 'D': case 'd':
            { return "1101"; }
        case 'E': case 'e':
            { return "1110"; }
        case 'F': case 'f':
            { return "1111"; }
        default: { return ""; }
    }
}

const char * hex_to_bin_16_chars(const char * cipher){
    const int cipher_length = strlen(cipher) > 16 ? 16 : strlen(cipher);
    
    char * binary = calloc(4 * cipher_length + 1, sizeof(char));

    for(int i = 0; i < cipher_length; i++){
        strcat(binary, hex_to_bin_char(cipher[i]));
    }

    return binary;
}


const char *get_next_hex_val(const char *val) {

    unsigned long int num = strtoul(val, NULL, 16);
    num++;

    char *next_val = calloc(17, sizeof(char));
    if (next_val == NULL) {
        perror("Memory allocation failed");
        return NULL;
    }

    snprintf(next_val, 17, "%lX", num);

    return next_val;
}

const char decrypt_bin(
    const char * cipher,
    const int cipher_length)
{
    // brute force
    // works also for 16 and whole strings 
    // lets say we have 8 chars long key for example
    // it would work like a | b | c | ... 26
    // aa | ab | a26 | ba | bb | bc | b26 | ca ... |2626
    // aaa | aab | aa26 | aba | a26a | 
    // if we have resoult with 1 in front we can skip due to unreadability
    
    // or maybe also work with hexes, lower range
    int key = 0;

    for(int i = 0; i < cipher_length; i+=2){
        char curr_letter[2] = { cipher[i],cipher[i+1] };

    }
    char decryption;
    return decryption;
}


const char * bin_to_ascii(const char * decrypted_cipher){
    char * result_ascii;

    return result_ascii;
}

int main (){

#define DEBUG 0
    const char * cipher = "C16B5ABF9340CE26031A0CE0C08FEAD3BBB2A182394F61D20DE4E8FADDE11EFE50EE0133BF4B46964691A18858870966939632CFB164CC172C16AB56817BE920D66048B88342DF3C4C0142FAD489FC9A9DB2E6";

    char sub_cipher[17] = { 0 };
    strncpy(sub_cipher, cipher, 16);
    decrypt_bin(sub_cipher, 16);

    const char * bin = hex_to_bin_16_chars(cipher);

#if DEBUG
    printf("Binary representation of the first 16 hex characters:\n%s\n", bin);
#endif

    // brute force magic
    // just dont print unreadable ascii
    // so i would habe range from 0 to 0111 1111
    // so we can ignore resoults if we have any resoult with 1 in front of byte
    //const char * decrypted = decrypt_bin(bin);
    
    free((void *)bin);
    
    const char *hex_val = "F1023C2312311111";
    const char *next_hex = get_next_hex_val(hex_val);

#if DEBUG
    printf("Next hex value is: %s\n", next_hex);  // This is just for checking
#endif
    free((void *)next_hex); // Free the allocated memory

    char a = 'C';
    char b = 'H';
    int i = a^b;
    printf("%d", i);
    return 0;
}
#endif





#if 0
#define BUFF_SIZE 256
char ** get_ciphers(FILE * file){
    char ** ciphers_list;

    

    return ciphers_list; 
}

int main ()
{
    FILE * input_file = fopen("../ciphers.txt", "r");

    char buf[BUFF_SIZE];

    const char * str = fgets(buf, BUFF_SIZE, input_file);

    printf("%s", str);
    
    char ** ciphers_list = get_ciphers(input_file);

    return 0;
}
#endif
