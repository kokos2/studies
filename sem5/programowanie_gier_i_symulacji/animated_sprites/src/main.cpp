#include "SFML/Graphics/Rect.hpp" 
#include "SFML/System/Vector2.hpp"
#include <SFML/Window.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <cstdio>

const sf::Vector2i window_size{800,600};

class Init_Sprite {
public:

    Init_Sprite(
        const std::string & path,
        const sf::Vector2i & grid) 
        : sprite_input_path(path), sprite_sheet_grid(grid)
    {
        texture.loadFromFile(path);
        sprite.setTexture(texture);

        sheet_size = 
            static_cast<sf::Vector2i>(texture.getSize());

        frame = {
            0,
            0,
            sheet_size.x/grid.x,
            sheet_size.y/grid.y
        };

        sprite.setPosition(
            window_size.x / 2.0f,
            window_size.y / 2.0f
        );
    }

    sf::Sprite sprite;
    sf::Vector2i sheet_size;
    const sf::Vector2i sprite_sheet_grid;

private:
    sf::Texture texture;
    sf::IntRect frame;
    const std::string sprite_input_path;
};

class Animation{
public:
    
    Animation() 
        : window(sf::VideoMode(window_size.x, window_size.y), "Lab1 animation"){}

    void run(){

        while (window.isOpen())
        {
            sf::Time current_frame_time = clock.restart();

            animation_time += current_frame_time;
            
            if(animation_time >= frame_time){
                current_frame = (current_frame + 1) % obj.sprite_sheet_grid.x;
                animation_time = sf::seconds(0.0f);

                sf::IntRect frame {
                    (obj.sheet_size.x/obj.sprite_sheet_grid.x) * current_frame,
                    0,
                    obj.sheet_size.x/obj.sprite_sheet_grid.x,
                    obj.sheet_size.y
                };

                obj.sprite.setTextureRect(frame);
            }
            
            render();
            process_event();
        }
    }

private:
    
    void render(){
        window.clear(sf::Color::Black);
        window.draw(obj.sprite);
        window.display();
    }

    void process_event(){
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
    }

    sf::RenderWindow window;

    int current_frame = 0;
    sf::Clock clock{};
    sf::Time animation_time = sf::seconds(0.0f);
    const sf::Time frame_time = sf::seconds(0.1f);

    const std::string sprites_dir = "../../sprites/";
    Init_Sprite obj{{sprites_dir + "sprite_sheet.png"}, {7,1}};
};

int main()
{
    Animation obj;
    obj.run();

    return 0;
}

