#pragma once


#include "SFML/System/Time.hpp"
#include "SFML/System/Vector2.hpp"

#include "SFML/Graphics/Sprite.hpp"


class Entity{
public:
    virtual void update(sf::Time delta) = 0;
    
    void update_position(const sf::Vector2f new_pos);

    sf::Sprite & get_sprite() {
        return sprite;
    }

protected:
    sf::Vector2f pos;
    sf::Sprite sprite;
};

