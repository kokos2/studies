#pragma once

#include "Move.hpp"
#include "SFML/Graphics/Texture.hpp"

class Enemy : public Move {
public:
    Enemy();
private:
    sf::Texture texture;
    sf::Vector2u position;
};

// Movement
// X axis
// Pipe G G G E G G G Pipe
// in this situation when on X we dont have player to target
// we just bounce between pipes
//
// Pipe G P G G E G Pipe
// if enemy "sees" player, it goes towards him

