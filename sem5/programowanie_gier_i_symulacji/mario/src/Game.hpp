#pragma once

#include "SFML/System/Vector2.hpp"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "SFML/Graphics/Rect.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>



#include <vector>


#include "Enums.hpp"
#include "Level.hpp"
#include "Player.hpp"
#include "Config.hpp"
#include "Entity.hpp"


class Game{

public :
    void run();
    void update();
    void init_sprite_textures();

    bool can_move(
        const sf::Vector2f sel_pos,
        const sf::Vector2i dir
    );
        
    void render_changes(
        const std::vector<Tile> & map
    );

public:
    // Singleton
    Game(const Game &) = delete;
    Game & operator = (const Game &) = delete;

    static Game& get_instance() {
        static Game instance; 
        return instance;
    }

private:
    Game();
    ~Game();
    
private:

    Level lvl;
    Player * player;

    sf::Texture bg, fg;
    sf::Sprite bg_sprite, fg_sprite;
    sf::IntRect tiles_bounds[TILESET_ELEMENTS];

    sf::RenderWindow window{};

    int lvl_number = 1;
    std::filesystem::path lvl_path = Levels::MAP[lvl_number];
    
    std::vector<std::unique_ptr<Entity>> entities;

    sf::Clock clock;

    sf::View camera{{0.0f,0.0f},{VIEW_SIZE.x,VIEW_SIZE.y}};
};

