#pragma once

#include "SFML/Graphics/Rect.hpp"
#include "SFML/System/Vector2.hpp"

#include "Utils.hpp"


struct Tile_with_rect{

    Tile_with_rect(const sf::Vector2u sel_pos) : 
        pos(sel_pos), rect(create_rect_from_pos(static_cast<sf::Vector2f>(sel_pos))) {}

    Tile_with_rect(const sf::Vector2f sel_pos) : 
        pos(sel_pos), rect(create_rect_from_pos(sel_pos)) {}

    const sf::Vector2u pos;
    const sf::FloatRect rect;
};

struct TileMapper{

    TileMapper(
        const sf::Vector2f provided_pos,
        const unsigned map_length,
        const std::vector<Tile> & map_data) : 
            sel_elem_id(pos_to_elem(static_cast<sf::Vector2u>(provided_pos), map_length)),
            type(map_data[sel_elem_id]),
            occupied_pos(iteration_to_pos(sel_elem_id, map_length)),
            rect(Tile_with_rect(occupied_pos).rect)
    {}

    const unsigned sel_elem_id;
    const Tile type;
    const sf::Vector2u occupied_pos;
    const sf::FloatRect rect;
};

