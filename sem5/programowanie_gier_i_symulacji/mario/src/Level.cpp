#include "Level.hpp"

#include "Utils.hpp"


#include <fstream>
#include <iostream>


bool Level::load_level_from_file(const std::filesystem::path & path){
    if (!std::filesystem::exists(path)) {
        std::cout << "Failed to load level: " << path << std::endl;
        return false;
    }
    if (std::filesystem::is_empty(path)) {
        std::cout << "Empty file: " << path << std::endl;
        return false;
    }

    std::ifstream file(path);
    map = parse_file(map, map_size, file, path);
    file.close();

    return true;
}
