#pragma once

#include "SFML/System/Vector2.hpp"
#include <SFML/Graphics/Color.hpp>

#include "Enums.hpp"

#include <unordered_map>

namespace Levels {

    constexpr const char* MAP[10] = {
        "../maps/lvl0.txt",
        "../maps/lvl1.txt",
        "../maps/lvl2.txt",
        "../maps/lvl3.txt",
        "../maps/lvl4.txt",
        "../maps/lvl5.txt",
        "../maps/lvl6.txt",
        "../maps/lvl7.txt",
        "../maps/lvl8.txt",
        "../maps/lvl9.txt"
    };

    constexpr const char* TILESET[10] ={
        "../sprites/tileset/lvl0.png",
        "../sprites/tileset/lvl1.png",
        "../sprites/tileset/lvl2.png",
        "../sprites/tileset/lvl3.png",
        "../sprites/tileset/lvl4.png",
        "../sprites/tileset/lvl5.png",
        "../sprites/tileset/lvl6.png",
        "../sprites/tileset/lvl7.png",
        "../sprites/tileset/lvl8.png",
        "../sprites/tileset/lvl9.png"
    };

} // namespace Levels


const sf::Color BACKGROUND_COLOR = {146,144,255};
constexpr float SCALE = 3.f;
constexpr float MOVE_SPEED = 7.5f;

///////////////////////////////////////////////////////////////////////////////
// tile
const sf::Vector2u TILE_SIZE = {16,16};
constexpr unsigned TILESET_COLUMNS = 7;
constexpr unsigned TILESET_ROWS = 2;
constexpr unsigned TILESET_ELEMENTS = TILESET_ROWS * TILESET_COLUMNS;

const sf::Vector2u HALF_TILE_SIZE = {
    TILE_SIZE.x/2,
    TILE_SIZE.y/2
};

const float CORRECT_SCALE_X = TILE_SIZE.x * SCALE;
const float CORRECT_SCALE_Y = TILE_SIZE.y * SCALE;
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// view 
constexpr unsigned SHOWN_TILES_WIDTH_AMOUNT = 27;
constexpr unsigned SHOWN_TILES_HEIGHT_AMOUNT = 17;
constexpr unsigned X_PAD = 2;

const sf::Vector2u VIEW_TILES_AMOUNT = {
    SHOWN_TILES_WIDTH_AMOUNT,
    SHOWN_TILES_HEIGHT_AMOUNT
};

const sf::Vector2f VIEW_SIZE = {
    SHOWN_TILES_WIDTH_AMOUNT * TILE_SIZE.x * SCALE,
    SHOWN_TILES_HEIGHT_AMOUNT * TILE_SIZE.y * SCALE
};
    
const sf::Vector2u VIEW_POSITION{2535,168};

constexpr unsigned VIEW_LEVEL = 8;
const float Y_LEVEL = VIEW_LEVEL * CORRECT_SCALE_Y;
///////////////////////////////////////////////////////////////////////////////

namespace Bounds {
    constexpr unsigned NONE = 0;
    constexpr unsigned GROUND = 1;
    constexpr unsigned BRICK = 2;
    constexpr unsigned QUESTION = 3;
    constexpr unsigned FLAG_TOP = 4;
    constexpr unsigned PIPE_UP_LEFT = 5;
    constexpr unsigned PIPE_UP_RIGHT = 6;
    constexpr unsigned RAMP = 7;
    constexpr unsigned PLAYER = 8;
    constexpr unsigned ENEMY = 9;
    constexpr unsigned FLAG_BODY = 10;
    constexpr unsigned FLAG_POINT = 11;
    constexpr unsigned PIPE_BODY_L = 12;
    constexpr unsigned PIPE_BODY_R = 13;
}; // namespace Bounds

const std::unordered_map<Tile, unsigned> tile_to_bounds = {
    {Tile::none,        Bounds::NONE},
    {Tile::ground,      Bounds::GROUND},
    {Tile::brick,       Bounds::BRICK},
    {Tile::question,    Bounds::QUESTION},
    {Tile::flag_top,    Bounds::FLAG_TOP},
    {Tile::pipe_up_l,   Bounds::PIPE_UP_LEFT},
    {Tile::pipe_up_r,   Bounds::PIPE_UP_RIGHT},
    {Tile::ramp,        Bounds::RAMP},
    // Player is keeped as just tile in map
    {Tile::player,      Bounds::NONE},
    {Tile::enemy,       Bounds::ENEMY},
    {Tile::flag_body,   Bounds::FLAG_BODY},
    {Tile::flag_point,  Bounds::FLAG_POINT},
    {Tile::pipe_body_l, Bounds::PIPE_BODY_L},
    {Tile::pipe_body_r, Bounds::PIPE_BODY_R}
};










