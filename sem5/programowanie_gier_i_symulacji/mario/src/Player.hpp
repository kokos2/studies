#pragma once

#include "SFML/Graphics/RenderWindow.hpp"
#include "SFML/System/Time.hpp"
#include "SFML/Graphics/Texture.hpp"

#include "Move.hpp"

class Player : public Move{
public:
    virtual void update(const sf::Time delta) final;
    virtual bool is_falling() final;

    void render(sf::RenderWindow & window);

    sf::Vector2f get_position() const {
        return pos;
    } 

    Player();

private:

    sf::Texture texture;
    int score = 0;
};

