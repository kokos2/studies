#include "Player.hpp"

#include "Game.hpp"
#include "Config.hpp"
#include "Utils.hpp"


#include "SFML/Graphics/RenderWindow.hpp"
#include "SFML/System/Vector2.hpp"

#include <SFML/Window/Keyboard.hpp>

#include <cmath>


Player::Player() {
    texture.loadFromFile("../sprites/single_player_test_hb.png");

    sprite.setTexture(texture);
    sprite.setScale(SCALE,SCALE);
    sprite.setOrigin(
        HALF_TILE_SIZE.y,
        HALF_TILE_SIZE.x
    );
}

// vec struct if pressed 2 keys
void Player::update(const sf::Time delta) {

    // depends on key we send animation
    sf::Vector2i dir{0,0};
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::H)) {
        dir.x = -1;
        sprite.setScale(-SCALE,SCALE);
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::L)) {
        dir.x = 1;
        sprite.setScale(SCALE,SCALE);
    }
    // allows diagonall movement
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::J)) {
        dir.y = 1;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::K)) {
        dir.y = -1;
    }

    const sf::Vector2f movement = {
        dir.x * delta.asSeconds() * MOVE_SPEED, 
        dir.y * delta.asSeconds() * MOVE_SPEED
    };

    const sf::Vector2f pos_to_be_checked = pos + movement;


    if (Game::get_instance().can_move(pos_to_be_checked, dir)) {
        pos = pos_to_be_checked;
    }

}

void Player::render(sf::RenderWindow & window){ 
    sprite.setPosition(
        pos.x * TILE_SIZE.x * SCALE,
        pos.y * TILE_SIZE.x * SCALE
    );

    window.draw(sprite);
}

bool Player::is_falling(){
    return true; 
}

