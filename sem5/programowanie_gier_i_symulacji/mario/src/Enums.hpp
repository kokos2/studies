#pragma once

enum class Tile : const char{
    none        = ' ',
    ground      = 'G',
    brick       = 'B',
    question    = 'Q',
    flag_top    = 'F',
    pipe_up_l   = '{',
    pipe_up_r   = '}',
    ramp        = 'R',
    player      = 'P',
    enemy       = 'E',
    flag_body   = '|',
    flag_point  = '.',
    pipe_body_l = '[',
    pipe_body_r = ']',
};

enum class Game_State {
    Beggining = 0,
    Pause,
    Loose,
    Win
};

