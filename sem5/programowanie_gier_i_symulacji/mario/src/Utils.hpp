#pragma once

#include "Enums.hpp"

#include "Level.hpp"
#include "SFML/Graphics/Rect.hpp"


#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderWindow.hpp>


#include <vector>
#include <filesystem>
#include <fstream>


void init_window(
    sf::RenderWindow & window,
    const sf::Vector2u & map_size
);

void print_pos_vec_float_ceil(const sf::Vector2f pos);

void print_map(
    const Level & lvl, 
    const sf::Vector2f sel_pos
);

sf::FloatRect create_rect_from_pos(const sf::Vector2f sel_pos);

///////////////////////////////////////////////////////////////////////////////
// tile
Tile char_to_tile(char tile);

std::vector<Tile> parse_file(
    std::vector<Tile> & map, 
    sf::Vector2u & map_size,
    std::ifstream & file,
    const std::filesystem::path & path
);


void set_tiles_bounds(
    sf::IntRect * bounds
);

int determine_tile(
    const std::vector<Tile> & map,
    const int pos
);


unsigned get_bounds_for_tile(Tile tile);
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// 1d to 2d
sf::Vector2u iteration_to_pos(
    const unsigned iteration, 
    const unsigned map_length
);

unsigned pos_to_elem(
    const sf::Vector2u pos, 
    const unsigned map_length
);
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// operators
template<typename T>
std::ostream & operator<<(std::ostream & os, const sf::Vector2<T> pos){

    return os << "pos x : " << pos.x << " pos y : " << pos.y << std::endl;
}

inline std::ostream & operator<<(std::ostream & os, const sf::FloatRect rect){
    return os << 
        "rect \ntop : " << rect.top << 
        "\nleft : " << rect.left << 
        "\nwidth : " << rect.width << 
        "\nheight : " << rect.height <<
    std::endl;
}
///////////////////////////////////////////////////////////////////////////////

