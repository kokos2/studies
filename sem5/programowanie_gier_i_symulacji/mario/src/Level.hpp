#pragma once

#include "Enums.hpp"


#include "SFML/System/Vector2.hpp"


#include <filesystem>
#include <vector>


class Level {
public:
    bool load_level_from_file(const std::filesystem::path & path);
    // load from memory(Tile * tile, sf::Vector2u size)

    Tile get_tile(int x, int y){
        return Tile::player;
    }
    
    sf::Vector2u get_map_size(){return map_size;}
    sf::Vector2u get_map_size() const {return map_size;}
    const std::vector<Tile> & get_map() const {return map;} 

private:
    sf::Vector2u map_size{};
    std::vector<Tile> map = {};
};

