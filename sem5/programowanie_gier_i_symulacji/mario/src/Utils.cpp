#include "Utils.hpp"

#include <iostream>
#include <cmath>

#include "Config.hpp"
#include "SFML/System/Vector2.hpp"

unsigned get_bounds_for_tile(Tile tile) {

    auto it = tile_to_bounds.find(tile);
    if (it != tile_to_bounds.end()) {
        return it->second; 
    }

    return Bounds::NONE;
}

Tile char_to_tile(char tile) {
    switch (tile) {
        case ' ': return Tile::none;
        case 'G': return Tile::ground;
        case 'B': return Tile::brick;
        case 'Q': return Tile::question;
        case 'F': return Tile::flag_top;
        case '{': return Tile::pipe_up_l;
        case '}': return Tile::pipe_up_r;
        case 'R': return Tile::ramp;
        case 'P': return Tile::player;
        case 'E': return Tile::enemy;
        case '|': return Tile::flag_body;
        case '.': return Tile::flag_point;
        case '[': return Tile::pipe_body_l;
        case ']': return Tile::pipe_body_r;
        default:  return Tile::none; 
    }
}

std::vector<Tile> parse_file(
    std::vector<Tile> & map, 
    sf::Vector2u & map_size,
    std::ifstream & file,
    const std::filesystem::path & path)
{
    std::string line;

    if (std::getline(file, line)) {
        map_size.x = std::stoi(line); 
    }

    if (std::getline(file, line)) {
        map_size.y = std::stoi(line);
    }

    while (std::getline(file, line)) { 

        for(const auto & elem : line){
            map.push_back(char_to_tile(elem));
        }
    }
    
    // file validation
    const unsigned map_xy_elements = map_size.x * map_size.y;
    if((map_xy_elements) != map.size()) {
        std::cout << 
            "Map file elements misplacement : " << path << "\n" << 
            "Loaded amount of elements : " << map.size() << "\n" << 
            "Declared elements : " << map_xy_elements <<
        std::endl;
    }

    return map;
}

void init_window(
    sf::RenderWindow & window,
    const sf::Vector2u & map_size) 
{
    window.create(
        sf::VideoMode(
            VIEW_SIZE.x,
            VIEW_SIZE.y
        ), "Mario",
        sf::Style::None
    );

    window.setPosition(static_cast<sf::Vector2i>(VIEW_POSITION));
    window.clear(BACKGROUND_COLOR);
}


void set_tiles_bounds(
    sf::IntRect * bounds)
{
    for (int row = 0; row < TILESET_ROWS; row++) {
        for (int col = 0; col < TILESET_COLUMNS; col++) {
            bounds[(row * TILESET_COLUMNS) + col] = sf::IntRect(
                col * TILE_SIZE.x,
                row * TILE_SIZE.y,
                TILE_SIZE.x,
                TILE_SIZE.y
            );
        }
    }
}

sf::Vector2u iteration_to_pos(
    const unsigned iteration, 
    const unsigned map_length)
{
    return sf::Vector2u{
        iteration % map_length,
        iteration / map_length
    };
}

//Tile pos_to_tile(const unsigned sel_pos){ }

unsigned pos_to_elem(
    const sf::Vector2u pos, 
    const unsigned map_length)
{
    return map_length * pos.y + pos.x;
}
 
void print_pos_vec_float_ceil(const sf::Vector2f pos){
    std::cout << "pos x : " << std::ceilf(pos.x) << " pos y : " << std::ceilf(pos.y) << std::endl;
}

void print_map(const Level & lvl, const sf::Vector2f sel_pos){
    const auto map = lvl.get_map();
    const auto map_width = lvl.get_map_size().x;

    for(int i = 0; const auto & elem : map){
        
        if ((i % map_width) == 0){
            std::cout << std::endl;
        }

        if ((std::floor(sel_pos.y) * map_width + std::floor(sel_pos.x)) == i){
            std::cout << "*";

        }else{
            std::cout << (char)elem;
        }

        i++;
    }
    std::cout << std::endl;
}

sf::FloatRect create_rect_from_pos(const sf::Vector2f sel_pos){
    return sf::FloatRect{
        sel_pos.x * CORRECT_SCALE_X,
        sel_pos.y * CORRECT_SCALE_Y,
        TILE_SIZE.x * SCALE,
        TILE_SIZE.y * SCALE
    };
}

