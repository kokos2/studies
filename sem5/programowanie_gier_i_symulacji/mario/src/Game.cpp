#include "Game.hpp"

#include "Config.hpp"
#include "SFML/System/Vector2.hpp"
#include "Utils.hpp"
#include "Helpers.hpp"

#include <SFML/Window/Event.hpp>

#include <memory>
#include <cmath>



void init_player_position(const Level & lvl, Player * player){
    auto map = lvl.get_map();

    for(unsigned i = 0; i < map.size(); i++){

        if(map[i] == Tile::player){

            const sf::Vector2f pos = static_cast<sf::Vector2f>(
                iteration_to_pos(i, lvl.get_map_size().x)
            );

            player->update_position(pos);

            return;
        }
    }
}

Game::~Game() {
    delete player;
}

Game::Game(){
   player = new Player();
   entities.push_back(std::make_unique<Player>());
}

bool collision_with_object(const Tile tile) {
    switch (tile) {
        case Tile::ramp:
        case Tile::ground:
        case Tile::brick:
        case Tile::question:
        case Tile::flag_top:
        case Tile::pipe_up_l:
        case Tile::pipe_up_r:
        case Tile::flag_body:
        case Tile::flag_point:
        case Tile::pipe_body_l:
        case Tile::pipe_body_r:
            return true; 
        case Tile::none:
            return false;
        //case Tile::coin:
        //some points
        default:
            return false;
    }
}


bool Game::can_move(
    const sf::Vector2f sel_pos,
    const sf::Vector2i dir)
{
    const unsigned diff = abs(dir.x) + abs(dir.y);

    // no move
    if(diff == 0){
        return true;
    }

    const auto & map = lvl.get_map();
    const unsigned map_length = lvl.get_map_size().x;

    for(int y = -1; y < 2; y++){
        for(int x = -1; x < 2; x++){

            // skip player
            if(x == 0 && y == 0){
                continue;
            }

            const TileMapper curr_tile(
                {sel_pos.x + x * 0.40f, sel_pos.y + y * 0.49f},map_length,map
            );

            if(collision_with_object(curr_tile.type)){
                return false;
            }
        }
    }

    print_map(lvl,sel_pos);

    return true;
}

void Game::init_sprite_textures(){
    fg.loadFromFile(Levels::TILESET[lvl_number]);
    fg_sprite.setTexture(fg);
    fg_sprite.setScale(SCALE,SCALE);
}


void Game::render_changes(
    const std::vector<Tile> & map)
{
    window.clear(BACKGROUND_COLOR);

    for(int i = 0; i < map.size(); i++){

        // skip background
        if(map[i] == Tile::none) {
            continue; 
        }
        
        const unsigned bounds = get_bounds_for_tile(map[i]);
        fg_sprite.setTextureRect(tiles_bounds[bounds]);

        const unsigned map_length = lvl.get_map_size().x;

        const sf::Vector2u tile_pos = iteration_to_pos(i, map_length);
        
        fg_sprite.setPosition(
            tile_pos.x * TILE_SIZE.x * SCALE,
            tile_pos.y * TILE_SIZE.y * SCALE
        );

        window.draw(fg_sprite);
    }
    
    player->render(window);

    window.display();
}

// visible like 15 Y at start
// we cant go back so we lock to view
// if we drop down we just dont render it and loose 
// pass direction, if we go rifht we update camera
//
void pad_camera(
    const sf::Vector2f pos, 
    const sf::Vector2f start_pos, 
    const sf::Vector2u map_size,
    sf::View & view)
{
    const bool need_to_pad_start = std::floor(pos.x) < (float)SHOWN_TILES_WIDTH_AMOUNT/2;
    const bool need_to_pad_finish = pos.x > map_size.x - (float)SHOWN_TILES_WIDTH_AMOUNT/2;

    if(need_to_pad_start){
        view.setCenter( 
            CORRECT_SCALE_X * ((start_pos.x - X_PAD)  + SHOWN_TILES_WIDTH_AMOUNT / 2),
            Y_LEVEL
        );
    } else if (need_to_pad_finish) {
        // stop camera
    } else {
        view.setCenter(
            pos.x * CORRECT_SCALE_X,
            Y_LEVEL
        );
    }
}

void Game::run(){

    if(!lvl.load_level_from_file(lvl_path)){
        return;
    }

    init_sprite_textures();
    init_window(window, lvl.get_map_size());
    set_tiles_bounds(tiles_bounds);
    init_player_position(lvl, player);
    const sf::Vector2f start_pos = player->get_position();

    while(window.isOpen()){

        sf::Event event{};
        while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed){
                window.close();
            }
        }

        sf::Time delta = clock.restart();
        
        player->update(delta);
        render_changes(lvl.get_map());
    
        pad_camera(
            player->get_position(), 
            start_pos,
            lvl.get_map_size(), 
            camera
        );

        window.setView(camera);
    }
}









