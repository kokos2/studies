#include <functions.hpp>
#include <array>
#include <iostream>
#include <filters.hpp>

void menu()
{
    const std::string tab = "        ";
    const std::string tabBig = "                     ";
}

class MoreWindows {
public:
    MoreWindows() = default;

    MoreWindows(
        const std::string & title, 
        const sf::Sprite & sprite, 
        const sf::Texture & texture, 
        sf::Vector2u size) 
        : savePath(title), sprite(sprite), texture(texture) 
    {
        this->sprite.setTexture(this->texture);
        window.create(sf::VideoMode(size.x, size.y), title, sf::Style::None);
        renderTexture.create(size.x, size.y);
    }

    void screenShot() const 
    {
        renderTexture.getTexture().copyToImage().saveToFile(savePath);
    }

    void run() {

        if (!window.isOpen()) { return; }

        sf::Event event;
        while (window.pollEvent(event)) 
        {
            if (event.type == sf::Event::Closed) { window.close(); }
        }

        renderTexture.clear(sf::Color::Black);
        renderTexture.draw(sprite);
        renderTexture.display();

        window.clear();
        sf::Sprite spr (renderTexture.getTexture());
        window.draw(spr);
        window.display();
    }

    bool isOpen() const {
        return window.isOpen();
    }

private:
    sf::Texture texture;
    const std::string savePath; 
    sf::RenderWindow window;
    sf::RenderTexture renderTexture;
    sf::Sprite sprite;
};

class Application 
{    
    void pushWindow(const std::string & title, const sf::Image & im, sf::Vector2u size) 
    {
        sf::Texture texture;
        texture.loadFromImage(im);
        sf::Sprite sprite;
        sprite.setTexture(texture);
    
        new (&windows[count++]) MoreWindows { title, sprite, texture, size };
    }

    void pushWindow(const std::string & title, const sf::Sprite & sprite, sf::Vector2u size) 
    {
        new (&windows[count++]) MoreWindows { title, sprite, *sprite.getTexture(), size };
    }
    
public:

    void run(const std::string & task, const std::string & operation)
    {
        sf::Image im0, im1, im2;
        im0.loadFromFile("../../images/hehe.jpg");
        im1.loadFromFile("../../images/palec.jpg");
        im2.loadFromFile("../../images/arch.jpg");
        
        int imgHeight = (im0.getSize().y);
        int imgWidth  = (im0.getSize().x);
        
        sf::Texture resultTexture;
        std::pair<int, int> tempSize = std::make_pair(imgWidth,imgHeight);

        /*
            need to ensure we work with binary ims
            this gets rid of compression modifications
        */
        im0 = binarize(im0);
        im1 = binarize(im1);
        im2 = binarize(im2, 40);

        auto& mD = mask3;
        auto& mE = mask3E;
        
        sf::Image dila = dilation(im0, mD);
        sf::Image eros = erosion(im0, mE);
        sf::Image open = dilation(eros, mD);
        sf::Image clos = erosion(dila, mE);


        // erosion && dilation
        if(task == "1") 
        {
            pushWindow("../images/1/dila.jpg", dila, dila.getSize());
            pushWindow("../images/1/eros.jpg", eros, eros.getSize());
            pushWindow("../images/default.jpg", im0, im0.getSize());
        }
        // open  : erosion  -> dilation
        // close : dilation -> erosion
        // open close
        else if(task == "2")
        {
            pushWindow("../images/2/open.jpg", open, open.getSize());
            pushWindow("../images/2/clos.jpg", clos, clos.getSize());
            pushWindow("../images/default.jpg", im0, im0.getSize());
        }
        // show 1 && 2
        else if(task == "3")
        {
            pushWindow("../images/3/dila.jpg", dila, dila.getSize());
            pushWindow("../images/3/eros.jpg", eros, eros.getSize());

            pushWindow("../images/3/open.jpg", open, open.getSize());
            pushWindow("../images/3/clos.jpg", clos, clos.getSize());
            
            pushWindow("../images/default.jpg", im0, im0.getSize());
        }
        // morph gradient
        else if(task == "4")
        {       
            sf::Image d = dilation(im2, mD);
            sf::Image e = erosion(im2, mE);
            
            sf::Image mG = morphGradient(e, d);
            pushWindow("../images/4/d.jpg", d, d.getSize());
            pushWindow("../images/4/e.jpg", e, e.getSize());
            pushWindow("../images/4/mG.jpg", mG, mG.getSize());
        }
        // erase dirt
        else if(task == "5")
        {
            sf::Image finger = erosion(conToNeg(im1), mask3);
            pushWindow("../images/5/finger.jpg", finger, finger.getSize());
            pushWindow("../images/5/default.jpg", conToNeg(im1), im1.getSize());
        }
        // dilation, gradient, erosion experimental
        else if(task == "6")
        {
            auto& mSmile = mask5test;
            auto& mcross = mask3cross;
            
            sf::Image dilSmile = dilation(im0, mSmile);
            pushWindow("../images/6/dilSmile.jpg", dilSmile, dilSmile.getSize());

            sf::Image gradient = morphGradientDilatation(im2, mask3);
            pushWindow("../images/6/gradient.jpg", gradient, gradient.getSize());
            
            sf::Image cross = erosion(im0, mask3cross);
            pushWindow("../images/6/cross.jpg", cross, cross.getSize());
        }        
        // open better
        else if(task == "7")
        {
            // bigger open
            sf::Image erosBig = erosion(im0, mask5);
            sf::Image openBig = dilation(erosBig, mask5);

            pushWindow("../images/openBig/openBig.jpg", openBig, openBig.getSize());
            pushWindow("../images/default.jpg", im0, im0.getSize());
            
        }
        else
        {   
            pushWindow("../images/default.jpg", im0, im0.getSize());
            std::cout << "default image shown" << std::endl;
        }

        while (true) 
        {
            std::size_t open = 0u;
            
            for (std::size_t i = 0u; i < count; i++) 
            {
                if (windows[i].isOpen()) { open++; }
            }

            if (open == 0) {
                break;
            }

            for (std::size_t i = 0u; i < count; i++) {
                windows[i].run();
            }
        }

        for (std::size_t i = 0u; i < count; i++) 
        {
            windows[i].screenShot();
        }
    }

private:
    std::array<MoreWindows, 10> windows = {};
    std::size_t count = 0;
};

int main (int argc, char* argv[])
{
    menu();

    if (argc != 3)
    {
        std::cerr << "Usage: ./zestawnr <task> <operation/all>" << std::endl;
        return 1;
    }

    std::string task(argv[1]);
    std::string operation(argv[2]);
    Application app;
    app.run(task, operation);

    return 0;
}
