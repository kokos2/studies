cmake_minimum_required(VERSION 3.25)
project(zestawnr7)

add_executable(zestawnr7 src/main.cpp)

target_compile_features(zestawnr7 PRIVATE cxx_std_17)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../functions ${CMAKE_CURRENT_BINARY_DIR}/functions)

target_include_directories(zestawnr7 PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../filters)

target_link_libraries(zestawnr7
    PRIVATE
        functions
)
