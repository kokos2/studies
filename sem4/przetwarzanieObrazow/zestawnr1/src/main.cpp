#include <functions.hpp>

class Application 
{
public:

    void run()
    {
        sf::Texture txt0, txt1;
        txt0.loadFromFile("../../images/arch.jpg");
        txt1.loadFromFile("../../images/kobieta1.jpg");

        sf::Image im0, im1;
        im0 = txt0.copyToImage();
        im1 = txt1.copyToImage();
        
        // TODO : argc argv choose task
        // Zad 1 - obraz + obrazSzarosc
        sf::Image grayImageOne  = grayScale(im0);
        sf::Image grayImageTwo  = grayScale(im0);
        //im0 = grayImageTwo; // helpful if loaded image is color, dont need to change img below
        
        // Zad 2 - czarny obraz + gradient
        sf::Image zeroMatrix = createZeroMatrix(sf::Vector2u(256, 256));
        sf::Image gradient   = createGradientIm(zeroMatrix);

        gradient.saveToFile("../images/gradient.jpg");

        // zad 3 - negatyw (szary + kolorowy)
        sf::Image negativeColor   = conToNeg(im0);
        sf::Image negativeGray    = conToNeg(im0);

        // zad 4 - wczytac 2 binarne 256x256 - porownac na AND OR XOR
        sf::Image binaryAnd  = binary(im0, im1, BitOp::And);
        sf::Image binaryOr   = binary(im0, im1, BitOp::Or);
        sf::Image binaryXor  = binary(im0, im1, BitOp::Xor);

        // zad 5 - wczytac 2 obrazy w skali szarosci - dla kazdego arit, cut, normal 
        sf::Image arithmeticAdd    = arit(im0, im1, AritOp::Add);
        sf::Image arithmeticSub    = arit(im0, im1, AritOp::Sub);
        sf::Image arithmeticMul    = arit(im0, im1, AritOp::Mul);
        sf::Image cutRangeAdd      = aritCutRange(im0, im1, AritOp::Add);
        sf::Image cutRangeSub      = aritCutRange(im0, im1, AritOp::Sub);
        sf::Image cutRangeMul      = aritCutRange(im0, im1, AritOp::Mul);
        
        #if 0 // check if pure white image

            // checks for values of multiplied cutted image
            // 15 x 15 could give not exacly pure white image
            uint8_t minValue = min_value(cutRangeMul);
            uint8_t maxValue = max_value(cutRangeMul);
            std::cout << "min : " << static_cast<uint32_t>(minValue) << std::endl;
            std::cout << "max : " << static_cast<uint32_t>(maxValue) << std::endl;

        #endif

        sf::Image normalizationAdd = norm(arithmeticAdd);
        sf::Image normalizationSub = norm(arithmeticSub);
        sf::Image normalizationMul = norm(arithmeticMul);

        // zad 6 - mnozenie obrazu szarego przez siebie z normalizacja
        sf::Image normalizationGrayImg = norm(arit(im0, im0, AritOp::Mul));

        sf::Texture resultTexture;
        resultTexture.loadFromImage(negativeColor);
        sf::Image resultImage = resultTexture.copyToImage();
        // overrides file each time program is ran
        resultImage.saveToFile("../images/currentOperation.jpg");
        sprite.setTexture(resultTexture);

        #if 1 // change size of rendered sprite // 0 is default image size

            constexpr int scale = 3.0f;
            const int imgHeight = (txt0.getSize().y)/scale;
            const int imgWidth  = (txt0.getSize().x)/scale;
            constexpr float scaleValue = 1.0f / scale;

            sprite.setScale(scaleValue, scaleValue); 
            sprite.setOrigin(sprite.getLocalBounds().width / scale, 
                             sprite.getLocalBounds().height/ scale);

            sprite.setPosition(imgWidth / scale, imgHeight / scale);

        #else
            const int imgHeight = (txt0.getSize().y);
            const int imgWidth  = (txt0.getSize().x);
        #endif

        window.create(sf::VideoMode(imgWidth, imgHeight), "", sf::Style::None);
        window.setPosition(sf::Vector2i(10,10));

        while (window.isOpen())
        {
            sf::Event event{};

            while (window.pollEvent(event))
            {
                if (event.type == sf::Event::Closed) 
                {
                    window.close();
                }
            }
            render();
        }
    }

    void render()
    {
        window.clear(sf::Color(0, 0, 0, 255));
        window.draw(sprite);
        window.display();
    }

private:
    sf::RenderWindow window;
    sf::Sprite sprite;
};

int main ()
{
    Application obj;
    obj.run();
    return 0;
}

// TODO : 
/*
    argc argv -> Choose task
    create more windows
*/
