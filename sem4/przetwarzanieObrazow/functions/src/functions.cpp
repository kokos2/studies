#include <functions.hpp>
#include <stack>
#include <map>
#include <filesystem>
#include <random>
#include <cstdint>
#include <limits>
#include <cmath>
#include <numeric>
#include <iostream>
#include <array>
#include <set>


////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Input Manipulations
    Task 2
        Task 4
            Separating Vector wich moves sprite - separateInputMoveValues
        Task 5
            Separating origin and angle - separateInputOriginAngle
*/
////////////////////////////////////////////////////////////////////////////////////////////////////



std::pair<int, int> separateInputMoveValues(
    const std::string & delimiter, 
    const std::string & op)
{
    std::pair<int, int> separatedInput;
    std::string tempInput = op;
    size_t delimiterPos = tempInput.find(delimiter);
    std::string xPart = tempInput.substr(0, delimiterPos);
    std::string yPart = tempInput.substr(delimiterPos + delimiter.length());
    separatedInput.first  = std::stoi(xPart);
    separatedInput.second = std::stoi(yPart);

    return separatedInput;
}

std::pair<std::string, int> separateInputOriginAngle(
    const std::string & delimiter, 
    const std::string & op)
{
    std::pair<std::string, int> separatedInput;
    std::string tempInput = op;
    size_t delimiterPos = tempInput.find(delimiter);
    std::string originPart = tempInput.substr(0, delimiterPos);
    std::string anglePart = tempInput.substr(delimiterPos + delimiter.length());
    separatedInput.first = originPart;
    separatedInput.second = std::stoi(anglePart);

    return separatedInput;
}



////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    debugging
*/
////////////////////////////////////////////////////////////////////////////////////////////////////



void printImValuesIfNotBin(const sf::Image & im)
{
    for (std::uint32_t y = 0; y < im.getSize().y; y++) {
        for (std::uint32_t x = 0; x < im.getSize().x; x++) {
            sf::Color p = im.getPixel(x, y);
            
            if(p.r != 255 && p.r != 0)
            {
                std::cout 
                    << "Y : " << y << " X : " <<  x << std::endl 
                    << "R : " << (int)p.r << " G : " << (int)p.g << " B : " << (int)p.b << std::endl 
                    << std::string(25, '=') << std::endl;  
            }
        }
    }
}

void printImValues(const sf::Image & im)
{
    for (std::uint32_t y = 0; y < im.getSize().y; y++) {
        for (std::uint32_t x = 0; x < im.getSize().x; x++) {
            sf::Color p = im.getPixel(x, y);
            
            std::cout 
                << "Y : " << y << " X : " <<  x << std::endl 
                << "R : " << (int)p.r << " G : " << (int)p.g << " B : " << (int)p.b << std::endl 
                << std::string(25, '=') << std::endl;  
        }
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    debugging
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

//rgb to bin

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 1

    --- Operations ---
    Grayscale
    Negative
    Zeros Matrix
    Gradient
    Binary 
    Arithmetic, Normalization, CutRange

*/
////////////////////////////////////////////////////////////////////////////////////////////////////



// 1 - Gray Image
sf::Image grayScale(const sf::Image & im)
{
    const sf::Vector2u imSize = im.getSize();
    sf::Image grayImage;
    grayImage.create(imSize.x,imSize.y);

    for (int y = 0; y < imSize.y; ++y)
    {
        for (int x = 0; x < imSize.x; ++x)
        {
            const sf::Color pixel = im.getPixel(x, y);
            const float grayValue = 
                0.2989f * pixel.r +
                0.5870f * pixel.g +
                0.1140f * pixel.b;

            grayImage.setPixel(x, y, sf::Color(grayValue, grayValue, grayValue));
        }
    }
    
    return grayImage;
}



// 2 - Create matrix filled with 0, then make it gradient
sf::Image createZeroMatrix(sf::Vector2u size)
{
    sf::Image zerosMatrix;
    zerosMatrix.create(size.x,size.y);

    return zerosMatrix;
}
sf::Image createGradientIm(sf::Image zerosMatrix)
{

    for (int y = 0; y < zerosMatrix.getSize().y; y++)
    {
        for (int x = 1; x < zerosMatrix.getSize().x; x++)
        {
            zerosMatrix.setPixel(x, y, sf::Color(x,x,x));
        }
        
    }
    
    return zerosMatrix;
}



// 3 - convert image to negative
sf::Image conToNeg(const sf::Image & im)
{
    const sf::Vector2u imSize = im.getSize();
    sf::Image negImage;
    negImage.create(imSize.x, imSize.y);

    for (int y = 0; y < imSize.y; ++y)
    {
        for (int x = 0; x < imSize.x; ++x)
        {
            sf::Color pixel = im.getPixel(x, y);

            pixel.r = 255 - pixel.r;
            pixel.g = 255 - pixel.g;
            pixel.b = 255 - pixel.b;

            negImage.setPixel(x, y, pixel);
        }
    }

    return negImage;
}



// 4 - Bitwise operations
sf::Image binary(const sf::Image & im0, const sf::Image & im1, 
                 const BitOp & op)
{
    const sf::Vector2u imSize = im0.getSize();
    sf::Image resIm;
    resIm.create(imSize.x,imSize.y);

    for(int y = 0; y < imSize.y; y++)
    {
        for (int x = 0; x < imSize.x; x++)
        {
            const sf::Color img1pixel = im0.getPixel(x, y);
            const sf::Color img2pixel = im1.getPixel(x, y);
            const int a = img1pixel == sf::Color::Black ? 1 : 0;
            const int b = img2pixel == sf::Color::Black ? 1 : 0;

            switch (op)
            {
            case BitOp::And :
                    resIm.setPixel(x, y,  a & b == 1 ? sf::Color::Black : sf::Color::White);
                break;
            case BitOp::Or :
                    resIm.setPixel(x, y,  a | b == 1 ? sf::Color::Black : sf::Color::White);
                break;
            case BitOp::Xor :
                    resIm.setPixel(x, y,  a ^ b == 1 ? sf::Color::Black : sf::Color::White);
                break;

            default:
                break;
            }
        }
    }

    return resIm;
}



// 5 - Arithmetic, normalization, cutRange
sf::Image arit(const sf::Image & im0, const sf::Image & im1,
               const AritOp & op)
{
    
    const sf::Vector2u imSize = im0.getSize();
    sf::Image resIm;
    resIm.create(imSize.x,imSize.y);
    
    for (int y = 0; y < imSize.y; y++)
    {
        for (int x = 0; x < imSize.x; x++)
        {
            sf::Color pixel1 = im0.getPixel(x, y);
            sf::Color pixel2 = im1.getPixel(x, y);

            switch (op)
            {
            case AritOp::Add :
                    pixel1.r += pixel2.r;
                    pixel1.g += pixel2.g;
                    pixel1.b += pixel2.b;
                break;
            
            case AritOp::Sub :
                    pixel1.r -= pixel2.r;
                    pixel1.g -= pixel2.g;
                    pixel1.b -= pixel2.b;
                break;

            case AritOp::Mul :
                    pixel1.r *= pixel2.r;
                    pixel1.g *= pixel2.g;
                    pixel1.b *= pixel2.b;
                break;
            
            default:
                break;
            }

            resIm.setPixel(x, y, pixel1);
        }
    }

    return resIm;
}

sf::Image aritCutRange(const sf::Image & im0, const sf::Image & im1,
                       const AritOp & op)
{
    const sf::Vector2u imSize = im0.getSize();
    sf::Image resIm;
    resIm.create(imSize.x,imSize.y);

    // clamps values between 0, 255 casting cuz need more than 255x255    
    #define OPERATOR(op, c)                                 \
        static_cast<std::uint8_t>(std::max(0, std::min(255, \
        static_cast<std::int32_t>(pixel1.c) op              \
        static_cast<std::int32_t>(pixel2.c)                 \
    )))

    for (int y = 0; y < imSize.y; y++)
    {
        for (int x = 0; x < imSize.x; x++)
        {
            const sf::Color pixel1 = im0.getPixel(x, y);
            const sf::Color pixel2 = im1.getPixel(x, y);
            
            switch (op)
            {
            case AritOp::Add :
                resIm.setPixel(x, y, sf::Color {OPERATOR(+, r), OPERATOR(+, g), OPERATOR(+, b)});
                break;
            case AritOp::Mul :
                resIm.setPixel(x, y, sf::Color {OPERATOR(*, r), OPERATOR(*, g), OPERATOR(*, b)});
                break;
            case AritOp::Sub :
                resIm.setPixel(x, y, sf::Color {OPERATOR(-, r), OPERATOR(-, g), OPERATOR(-, b)});
                break; 
            default:
                break;
            }
        }
    }  

    return resIm;
}

#if 0 // changed in task 2 cuz RGB needs
std::uint8_t min_value(const sf::Image & im) 
{
    std::uint8_t ret = std::numeric_limits<std::uint8_t>::max();

    for (int y = 0; y < im.getSize().y; y++) {

        for (int x = 0; x < im.getSize().x; x++) {

            const sf::Color p = im.getPixel(x, y);
            if (p.r < ret) {
                ret = p.r;
            }
        }
    }
    
    return ret;
}

std::uint8_t max_value(const sf::Image & im) 
{
    std::uint8_t ret = std::numeric_limits<std::uint8_t>::min(); // returns min value of uint8

    for (int y = 0; y < im.getSize().y; y++) {

        for (int x = 0; x < im.getSize().x; x++) {

            const sf::Color p = im.getPixel(x, y);
            if (p.r > ret) { // cheks only 1 channel cuz grayScale
                ret = p.r;
            }
        }
    }

    return ret;
}

sf::Image norm(const sf::Image & im)
{
    const sf::Vector2u imSize = im.getSize();
    sf::Image resIm;
    resIm.create(imSize.x,imSize.y);
    
    const std::uint8_t minValue = min_value(im);
    const std::uint8_t maxValue = max_value(im);

    for (int y = 0; y < imSize.y; y++)
    {
        for (int x = 0; x < imSize.x; x++)
        {
            const sf::Color p = im.getPixel(x, y);
            const std::uint8_t scaled = 255 * (p.r - minValue) / (maxValue - minValue);
            resIm.setPixel(x, y, sf::Color { scaled, scaled, scaled });
        }
    }

    return resIm;
}
#endif



////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 2

    --- Operations ---
    norm - changed min/max to rgb
*/
////////////////////////////////////////////////////////////////////////////////////////////////////



// 1 && 5 Rotate
Corner convertToEnum(std::string & corner)
{
    Corner c;
         if(corner == "LD") c = Corner::LD;
    else if(corner == "LU") c = Corner::LU;
    else if(corner == "RD") c = Corner::RD;
    else if(corner == "RU") c = Corner::RU;
    else c = Corner::CE;
    return c; // could return from if's
}

sf::Vector2f getOrigin(const sf::Image &im, Corner corner)
{
    sf::Vector2f origin;
    sf::Vector2u size = im.getSize();
    
    switch (corner)
    {
        case Corner::LU:
            origin = sf::Vector2f(0, 0);
            break;
        case Corner::RU:
            origin = sf::Vector2f(static_cast<float>(size.x), 0);
            break;
        case Corner::LD:
            origin = sf::Vector2f(0, static_cast<float>(size.y));
            break;
        case Corner::RD:
            origin = sf::Vector2f(static_cast<float>(size.x), static_cast<float>(size.y));
            break;
        case Corner::CE:
            origin = sf::Vector2f((im.getSize().x)/2, (im.getSize().y)/2);
            break;
    }

    return origin;
}

float angleRad(float angle)
{
    return angle * (M_PI/180.0f);
}

sf::Vector2u windowSizeRotation(
    const sf::Vector2u size, 
    sf::Vector2f origin, float angle) 
{
    sf::Vector2f p[4];

    for (int i = 0; i < 4; ++i) 
    {
        /*
            x and y defines squares
            we have square of points 0,5f; 0,5f
            each corner have diffrent coordinates
        */ 
        float x = (i % 2 == 0 ? -0.5f : 0.5f) * size.x;
        float y = (i < 2 ? -0.5f : 0.5f) * size.y;
        
        /*
            moves and rotates previously calculated vertices
            https://stackoverflow.com/questions/2259476/rotating-a-point-about-another-point-2d
        */
        p[i] = 
        {
            static_cast<float>(origin.x + cos(angle) * x - sin(angle) * y),
            static_cast<float>(origin.y + sin(angle) * x + cos(angle) * y)
        };
    }

    // Find min and max values
    float minX = std::min({p[0].x, p[1].x, p[2].x, p[3].x});
    float minY = std::min({p[0].y, p[1].y, p[2].y, p[3].y});
    float maxX = std::max({p[0].x, p[1].x, p[2].x, p[3].x});
    float maxY = std::max({p[0].y, p[1].y, p[2].y, p[3].y});

    // Returns size of bounding box for new sprite
    return sf::Vector2u(static_cast<std::uint32_t>(maxX - minX), 
                        static_cast<std::uint32_t>(maxY - minY));
}

sf::Image rotate(
    const sf::Image &im, 
    sf::Vector2f origin, 
    float angle, 
    sf::Vector2u resultImSize) 
{
    const sf::Vector2u imSize = im.getSize();
    sf::Image resIm;
    // Ensure the background is transparent (this shows that rotated img is bigger)
    resIm.create(resultImSize.x, resultImSize.y); 

    // caches values of given angle
    const float cos_a = cos(angle);
    const float sin_a = sin(angle);

    // Offset 
    /*
        Calculates the offset of given picture and result one
        we have to take into acount situation where one or both
        dimensions of the result image are smaller then their
        equivalents in the original one therefore the ternary
        operator, the then branch handles case where original 
        image dimensions are bigger than the result one and the
        else branch handles the opposite case
    */
    const sf::Vector2f offset 
    {
        resultImSize.x > imSize.x ? 
        (resultImSize.x - imSize.x) / 2.0f : (imSize.x - resultImSize.x) / -2.0f,

        resultImSize.y > imSize.y ? 
        (resultImSize.y - imSize.y) / 2.0f : (imSize.y - resultImSize.y) / -2.0f
    };

    for (unsigned y = 0; y < imSize.y; ++y) {
        for (unsigned x = 0; x < imSize.x; ++x) {

            // Translate coordinates to origin / current pixel cords
            const float translatedX = x - origin.x;
            const float translatedY = y - origin.y;

            // Apply rotation - same equasion from previous func 
            // calculates position of pixel after rotation
            const float rotatedX = origin.x + cos_a * translatedX - sin_a * translatedY;
            const float rotatedY = origin.y + sin_a * translatedX + cos_a * translatedY;

            // Translate coordinates back with the offset to center / cords pixel after origin
            const int finalX = static_cast<int>(rotatedX + offset.x);
            const int finalY = static_cast<int>(rotatedY + offset.y);

            // Check if within the new image bounds and set pixel
            if (finalX >= 0 && finalX < static_cast<int>(resultImSize.x) &&
                finalY >= 0 && finalY < static_cast<int>(resultImSize.y)) 
            {
                resIm.setPixel(finalX, finalY, im.getPixel(x, y));
            }
        }
    }
    
    return resIm;
}



// 2 Mirror
sf::Image mirrorReflection(const sf::Image & im, const Dimension & op)
{
    const sf::Vector2u imSize = im.getSize();
    sf::Image resIm;
    resIm.create(imSize.x,imSize.y);

    for(int y = 0; y < imSize.y; y++)
    {
        for (int x = 0; x < imSize.x; x++)
        {
            const sf::Color pixel = im.getPixel(x, y);

            switch (op)
            {
                // mirror 
                /*
                    Sets new pixel cords based on loop iteration
                */
                case Dimension::Horizontal:
                    resIm.setPixel(imSize.x - x - 1, y, pixel);
                    break;
                case Dimension::Vertical:
                    resIm.setPixel(x, imSize.y - y - 1, pixel);
                    break;
                default:
                    break;
            }
        }
    }

    return resIm;
}



// 3 Bigger window, sprite center
std::pair <int,int> sizeOfBiggerWindowOutsideSprite(
    int xMul, int yMul, 
    int oldWidth, int oldHeight)
{
    std::pair<int, int> newSize;

    newSize.first  = oldWidth * xMul;
    newSize.second = oldHeight * yMul;

    return newSize;
}

std::pair <int,int> spriteInMiddleOfBiggerWindow(
    int sizeX, int sizeY, 
    int imgWidth, int imgHeight)
{
    std::pair<int ,int> newOffset;

    newOffset.first  = (sizeX - imgWidth ) / 2;
    newOffset.second = (sizeY - imgHeight) / 2;

    return newOffset;
} 

std::pair<int, int> resizeWindowAndMoveSprite(
    int xMul, int yMul, 
    int oldWidth, int oldHeight, 
    sf::Sprite & sprite)
{
    std::pair<int, int> newSize = 
    sizeOfBiggerWindowOutsideSprite(xMul, yMul, oldWidth, oldHeight);

    std::pair<int, int> offset  = 
    spriteInMiddleOfBiggerWindow(newSize.first, newSize.second, oldWidth, oldHeight);
    
    sprite.setPosition(offset.first, offset.second);
    
    return newSize;
}



// 4 Move sprite
void moveSprite(sf::Sprite & sprite, float offsetX, float offsetY)
{
    sprite.move(offsetX, -offsetY);
}

// 6 arit, norm, cutRange in task 1
sf::Color min_values(const sf::Image & im)
{
    sf::Color ret(255,255,255,255);

    for (int y = 0; y < im.getSize().y; y++) 
    {
        for (int x = 0; x < im.getSize().x; x++) 
        {
            const sf::Color p = im.getPixel(x, y);

            if (p.r < ret.r) { ret.r = p.r; }
            if (p.g < ret.g) { ret.g = p.g; }
            if (p.b < ret.b) { ret.b = p.b; }
        }
    }
    
    return ret;
}

sf::Color max_values(const sf::Image & im)
{
    sf::Color ret(0,0,0,255);

    for (int y = 0; y < im.getSize().y; y++) 
    {
        for (int x = 0; x < im.getSize().x; x++) 
        {
            const sf::Color p = im.getPixel(x, y);

            if (p.r > ret.r) { ret.r = p.r; }
            if (p.g > ret.g) { ret.g = p.g; }
            if (p.b > ret.b) { ret.b = p.b; }
        }
    }
    
    return ret;
}

// files loaded into normalization
/*
    needs to load png/bmp file cuz loading jpg encoding 
    seems to have some kind of artifacts due to compression
*/
sf::Image norm(const sf::Image & im)
{
    const sf::Vector2u imSize = im.getSize();
    sf::Image resIm;
    resIm.create(imSize.x, imSize.y);
    
    sf::Color minValues = min_values(im);
    sf::Color maxValues = max_values(im);

    for (int y = 0; y < imSize.y; y++) 
    {
        for (int x = 0; x < imSize.x; x++) 
        {
            const sf::Color p = im.getPixel(x, y);

            sf::Color scaled = sf::Color(0,0,0,255);

            if (maxValues.r != minValues.r) {
                scaled.r = 255 * (p.r - minValues.r) / (maxValues.r - minValues.r);
            }

            if (maxValues.g != minValues.g) {
                scaled.g = 255 * (p.g - minValues.g) / (maxValues.g - minValues.g);
            }

            if (maxValues.b != minValues.b) {
                scaled.b = 255 * (p.b - minValues.b) / (maxValues.b - minValues.b);
            }

            resIm.setPixel(x, y, sf::Color { scaled.r, scaled.g, scaled.b });
        }
    }

    return resIm;
}



////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 3

    --- Operations ---
    scaling reproduction method
    Rotate corner did lab2
    Skew angle, basis
    greenscreen
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

// scaling reproduction method

sf::Vector2u windowSizeScale(const sf::Image & im, const sf::Vector2f scale)
{
    const sf::Vector2u imSize = im.getSize();
    sf::Vector2u resSize = {imSize.x * scale.x, imSize.y * scale.y};
    return resSize;
}

sf::Image scaleIm(const sf::Image & im, const sf::Vector2u scaledImSize, const sf::Vector2f scale)
{
    const sf::Vector2u imSize = im.getSize();
    
    sf::Image resIm;
    resIm.create(scaledImSize.x, scaledImSize.y);

    for (unsigned y = 0; y < imSize.y; ++y) {
        for (unsigned x = 0; x < imSize.x; ++x) {

            const float finalX = x * scale.x;
            const float finalY = y * scale.y;

            // Check if within the new image bounds and set pixel
            if (finalX >= 0 && finalX < static_cast<int>(scaledImSize.x) &&
                finalY >= 0 && finalY < static_cast<int>(scaledImSize.y)) 
            {
                resIm.setPixel(finalX, finalY, im.getPixel(x, y));
            }
        }
    }

    return resIm;
}

sf::Image fillGaps(const sf::Image & im, sf::Vector2u desired_size)
{
    sf::Image resIm;
    resIm.create(desired_size.x, desired_size.y);

    // Calulates ratio to propely sample pixel
    const float
        x_r = im.getSize().x / static_cast<float>(desired_size.x),
        y_r = im.getSize().y / static_cast<float>(desired_size.y);


    for (std::uint32_t y = 0u; y < desired_size.y; y++) 
    {
        // Calculate nearest neighbour in Y direction, adding 0.5f to floor variable
        const std::uint32_t n_y = static_cast<std::uint32_t>(y_r * y + 0.5f);

        for (std::uint32_t x = 0u; x < desired_size.x; x++) 
        {
            const std::uint32_t n_x = static_cast<std::uint32_t>(x_r * x + 0.5f);

            resIm.setPixel(
                x,
                y,
                im.getPixel(
                    n_x,
                    n_y
                )
            );
        }
    }

    return resIm;    
}

// Skew angle, basis

sf::Vector2u windowSizeSkew(
    const sf::Vector2u size, 
    Basis basis, float angle)
{
    sf::Vector2f p[4];
    sf::Vector2f origin {0.0f, 0.0f};

    for (int i = 0; i < 4; ++i) 
    {
        /*
            x and y defines squares
            we have square of points 0,5f; 0,5f
            each corner have diffrent coordinates
        */ 
        float x = (i % 2 == 0 ? -0.5f : 0.5f) * size.x;
        float y = (i < 2 ? -0.5f : 0.5f) * size.y;
        
        /*
            moves and skews previously calculated vertices
            https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/skew#ay
        */
        p[i] =
        {
            static_cast<float>(origin.x + x - tan(angle) * y * 
                (basis == Basis::Up || basis == Basis::Down)),

            static_cast<float>(origin.y + tan(angle) * x * 
                (basis == Basis::Left || basis == Basis::Right) + y)
        };
    }

    // Find min and max values
    float minX = std::min({p[0].x, p[1].x, p[2].x, p[3].x});
    float minY = std::min({p[0].y, p[1].y, p[2].y, p[3].y});
    float maxX = std::max({p[0].x, p[1].x, p[2].x, p[3].x});
    float maxY = std::max({p[0].y, p[1].y, p[2].y, p[3].y});

    // Returns size of bounding box for new sprite
    return sf::Vector2u(static_cast<std::uint32_t>(maxX - minX), 
                        static_cast<std::uint32_t>(maxY - minY));
}

sf::Image skew(
    const sf::Image & im, 
    sf::Vector2u resultImSize,
    float angle, 
    Basis basis)
{
    const sf::Vector2u imSize = im.getSize();
    sf::Image resIm;
    resIm.create(resultImSize.x, resultImSize.y, sf::Color::Transparent); 
    
    // caches values of given angle
    const float tan_a = tan(angle);

    // Origin
    /*
        Calculates the origin point for the skew operation
    */
    const sf::Vector2f origin = 
    {
        static_cast<float>(im.getSize().y * tan(angle) * 
            (basis == Basis::Up || basis == Basis::Down)),

        static_cast<float>(im.getSize().x * tan(angle) * 
            (basis == Basis::Left || basis == Basis::Right))
    };

    // Offset 
    /*
        Calculates the offset of given picture and result one
        based on the past angle we need to calculate shift val
        for either of the axes, we do so by using the tan func
        and appropriate dimension of the image
    */
    const sf::Vector2f offset = 
    {
        (angle < 0.0f ? -1.0f :  0.0f) * (im.getSize().y * tan(angle)) * 
            (basis == Basis::Up   || basis == Basis::Down), 

        (angle < 0.0f ? -2.0f : -1.0f) * (im.getSize().x * tan(angle)) * 
            (basis == Basis::Left || basis == Basis::Right)
    };

    for (unsigned y = 0; y < imSize.y; ++y) {
        for (unsigned x = 0; x < imSize.x; ++x) {

            // Translate coordinates to origin / current pixel cords
            const float translatedX = x - origin.x;
            const float translatedY = y - origin.y;

            // Apply skew - same equasion from previous func 
            // calculates position of pixel after rotation
            const float skewedX = origin.x + x - tan(angle) * y * 
                (basis == Basis::Up || basis == Basis::Down);

            const float skewedY = origin.y + tan(angle) * x * 
                (basis == Basis::Left || basis == Basis::Right) + y;

            // Translate coordinates back with the offset to center 
            // cords pixel after origin
            const int finalX = static_cast<int>(skewedX + offset.x);
            const int finalY = static_cast<int>(skewedY + offset.y);

            // Check if within the new image bounds and set pixel
            if (finalX >= 0 && finalX < static_cast<int>(resultImSize.x) &&
                finalY >= 0 && finalY < static_cast<int>(resultImSize.y)) 
            {
                resIm.setPixel(finalX, finalY, im.getPixel(x, y));
            }
        }
    }
    
    return resIm;
}


// greenscreen
std::array <sf::Vector2f, 4> findGreenVertices(
    const sf::Image & im,
    uint8_t threshold, 
    sf::Color greenVal)
{
    const sf::Vector2u imSize = im.getSize();

    // -1 cuz we iterates from 0
    std::uint32_t min_x = imSize.x - 1u, max_x = 0u;
    std::uint32_t min_y = imSize.y - 1u, max_y = 0u;

    for (std::uint32_t y = 0u; y < imSize.y; y++) {
        for (std::uint32_t x = 0u; x < imSize.x; x++) {

            const sf::Color p = im.getPixel(x, y);

            // skips if color isnt target
            if (std::abs(greenVal.r - p.r) > threshold ||
                std::abs(greenVal.g - p.g) > threshold ||
                std::abs(greenVal.b - p.b) > threshold) 
            {
                continue;
            }
            
            if (x < min_x) { min_x = x; } // Left
            if (x > max_x) { max_x = x; } // Right

            if (y < min_y) { min_y = y; } // Down
            if (y > max_y) { max_y = y; } // Up
        }
    }

    // Cords
    return {
        sf::Vector2f { min_x, min_y }, // LD
        sf::Vector2f { max_x, min_y }, // RD
        sf::Vector2f { max_x, max_y }, // RU
        sf::Vector2f { min_x, max_y }  // LU
    };
}

sf::Image fillGreenScreen(const sf::Image & greenScreen, const sf::Image & overlay)
{
    sf::Image resIm;
    resIm.create(greenScreen.getSize().x, greenScreen.getSize().y);
    resIm.copy(greenScreen, 0, 0);
    const sf::Vector2u imSize = overlay.getSize();
    const auto vertices = findGreenVertices(greenScreen, 50, sf::Color(82,254,82));

    const sf::Vector2f desired_size = 
    {
        vertices[1].x - vertices[0].x,
        vertices[3].y - vertices[0].y
    };

    const float
        x_r = overlay.getSize().x > 1 ? static_cast<float>((overlay.getSize().x - 1) / (desired_size.x - 1)) : 0,
        y_r = overlay.getSize().y > 1 ? static_cast<float>((overlay.getSize().y - 1) / (desired_size.y - 1)) : 0;

    //std::cout << "x_r : " << x_r << " y_r : " << y_r << std::endl;

    for (std::uint32_t y = 0; y < desired_size.y; y++) {
        for (std::uint32_t x = 0; x < desired_size.x; x++) {

            const float
                x_l = std::floor(x_r * x), x_h = std::ceil(x_r * x),
                y_l = std::floor(y_r * y), y_h = std::ceil(y_r * y);

            const float
                x_w = (x_r * x) - x_l,
                y_w = (y_r * y) - y_l;

            const sf::Color
                a = overlay.getPixel(x_l, y_l),
                b = overlay.getPixel(x_h, y_l),
                c = overlay.getPixel(x_l, y_h),
                d = overlay.getPixel(x_h, y_h);

            const auto f = [x_w, y_w](std::uint8_t a, std::uint8_t b, std::uint8_t c, std::uint8_t d) {
                return
                    a * (1 - x_w) * (1 - y_w) +
                    b * x_w * (1 - y_w) +
                    c * y_w * (1 - x_w) +
                    d * x_w * y_w;
            };

            resIm.setPixel(
                vertices[0].x + x,
                vertices[0].y + y,
                sf::Color {
                    f(a.r, b.r, c.r, d.r),
                    f(a.g, b.g, c.g, d.g),
                    f(a.b, b.b, c.b, d.b),
                    f(a.a, b.a, c.a, d.a)
                }
            ); 
        }
    }

    return resIm;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 4

    --- Operations ---
    masks in header in task3
    additional borders
    noise peper salt
    gaussian noise
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

sf::Image noisePeperSalt(
    const sf::Image &im, 
    const float frequency) 
{
    const sf::Vector2u imSize = im.getSize();
    sf::Image resIm = im; 
    
    std::random_device rd; // random device
    static std::mt19937 gen(rd()); // gen is Mersene Twister pseudo-random generator
    std::uniform_int_distribution<> distr(0, 1); // black || white
    std::uniform_int_distribution<> xdistr(0, imSize.x - 1);
    std::uniform_int_distribution<> ydistr(0, imSize.y - 1);

    // iterates through loop, based on frequency and amount of pixels in the image
    for (std::size_t k = 0; k < static_cast<std::size_t>(frequency * imSize.x * imSize.y); ++k) {
        const auto x = xdistr(gen);
        const auto y = ydistr(gen);
        const auto color = distr(gen) > 0 ? sf::Color::White : sf::Color::Black;
        resIm.setPixel(x, y, color);
    }

    return resIm;
}

// average pixel value
const std::uint8_t getMean(const sf::Image &im)
{
    const sf::Vector2u imSize = im.getSize();
    uint32_t sum = 0; 

    for (std::uint32_t y = 0u; y < imSize.y; y++) {
        for (std::uint32_t x = 0u; x < imSize.x; x++) {
            sum += im.getPixel(x,y).r;
        }
    }

    return sum/(imSize.y * imSize.x);
}

// represents the average distance of each pixel from the mean value
const float standardDeviation(const sf::Image &im)
{
    const uint8_t mean = getMean(im);
    const sf::Vector2u imSize = im.getSize();
    uint32_t sum = 0;

    for (std::uint32_t y = 0u; y < imSize.y; y++) {
        for (std::uint32_t x = 0u; x < imSize.x; x++) {
            sum += std::pow(im.getPixel(x, y).r - mean, 2);
        }
    }

    return sqrt(sum/(imSize.x * imSize.y));
}

sf::Image gaussianNoise(
    const sf::Image &im, 
    const float frequency)
{
    const sf::Vector2u imSize = im.getSize();
    sf::Image resIm = im; 

    const uint8_t mean = getMean(im);
    const float dev = standardDeviation(im);

    std::random_device rd;
    static std::mt19937 gen(rd());
    std::normal_distribution<double> dist(mean, dev);
    std::uniform_int_distribution<> xdistr(0, imSize.x - 1);
    std::uniform_int_distribution<> ydistr(0, imSize.y - 1);

    for (std::size_t k = 0; k < static_cast<std::size_t>(frequency * imSize.x * imSize.y); ++k) {
        const auto x = xdistr(gen);
        const auto y = ydistr(gen);        
        const auto color = static_cast<uint8_t>(dist(gen));
        resIm.setPixel(x, y, sf::Color{color, color, color});
    }

    return resIm;
}


sf::Image additionalBorders(
    const sf::Image & im,
    const std::size_t maskSize)
{
    const unsigned adSize = maskSize / 2;

    const sf::Vector2u imSize = im.getSize();
    sf::Vector2u newSize = sf::Vector2u{
        imSize.x + adSize * 2,
        imSize.y + adSize * 2
    };

    sf::Image resIm;
    resIm.create(newSize.x, newSize.y);
    
    // add borders 
    for (std::uint32_t y = 0u; y < imSize.y; y++){
        for (std::uint32_t x = 0u; x < imSize.x; x++){
            
            // Left
            if(x < adSize)
            {
                const sf::Color p = im.getPixel(0, y);
                resIm.setPixel(
                    x, y+adSize, p
                );

            // Right
            } else if (x >= (imSize.x - adSize)) {

                const sf::Color p = im.getPixel(imSize.x - 1, y);
                resIm.setPixel(
                    x + maskSize - 1, y + adSize, p
                );
            }
            
            // Up 
            if (y < adSize) {

                const sf::Color p = im.getPixel(x, 0);
                resIm.setPixel(
                    x + adSize, y, p
                );

            // Down
            } else if (y >= (imSize.y - adSize)) {

                const sf::Color p = im.getPixel(x, imSize.y - 1);
                resIm.setPixel(
                    x + adSize, y + maskSize -1, p
                );
            }
            
            const sf::Color p = im.getPixel(x, y);
            resIm.setPixel(x + adSize, y + adSize, p);
        }
    }
    
    // fill corners
    for (std::uint8_t y = 0u; y < adSize; y++){
        for (std::uint8_t x = 0u; x < adSize; x++){

            resIm.setPixel(
                x, y, im.getPixel(0, 0)
            );

            resIm.setPixel(
                newSize.x - adSize + x, y, im.getPixel(imSize.x - 1, 0)
            );

            resIm.setPixel(
                x, newSize.y - adSize + y, im.getPixel(0, imSize.y - 1)
            );

            resIm.setPixel(
                newSize.x - adSize + x, 
                newSize.y - adSize + y, 
                im.getPixel(imSize.x - 1, imSize.y - 1)
            );
        }
    }

    return resIm;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 5

    --- Operations ---
    any new operations
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 6

    --- Operations ---
    Median masks in header 
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 7

    --- Operations ---
    erosion
    dilatation 
    both in header

    morphological gradient
    binarize
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

// binarize needed cuz file compression
sf::Image binarize(
    const sf::Image & im, 
    std::uint8_t threshold) 
{
    sf::Image result;
    sf::Vector2u size = im.getSize();
    result.create(size.x, size.y, sf::Color::Black);

    for (std::uint32_t y = 0; y < size.y; y++) {
        for (std::uint32_t x = 0; x < size.x; x++) {
            sf::Color p = im.getPixel(x, y);
            
            result.setPixel(
                x, y, 
                p.r > threshold ? sf::Color::White : sf::Color::Black
            );
        }
    }

    return result;
}

bool containsTarget(
    const sf::Image & im, 
    const sf::Vector2u pos, 
    const int mask, 
    const sf::Color target)
{
    const sf::Vector2u imSize = im.getSize();
    
    for (int dy = -mask; dy <= mask; ++dy) {
        for (int dx = -mask; dx <= mask; ++dx) {
            
            int imX = std::clamp(int(pos.x) + dx, 0, int(imSize.x) - 1);
            int imY = std::clamp(int(pos.y) + dy, 0, int(imSize.y) - 1);

            if(im.getPixel(imX,imY) == target) return true;
        }
    }

    return false;
}

sf::Image morphGradient(
    const sf::Image & dilated,
    const sf::Image & eroded)
{
    const sf::Vector2u imSize = dilated.getSize();
    sf::Image resIm; 
    resIm.create(imSize.x, imSize.y);

    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {

            const sf::Color d = dilated.getPixel(x, y);
            const sf::Color e = eroded.getPixel(x, y);
            
            resIm.setPixel(x,y, 
                sf::Color{
                    static_cast<sf::Uint8>(d.r - e.r),
                    static_cast<sf::Uint8>(d.g - e.g),
                    static_cast<sf::Uint8>(d.b - e.b)
                }
            );
            
        }
    }

    return resIm;
}

    

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 8

    --- Operations ---
    conditional erosion
    thinning
    thickening
    detect end points
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

// all i header
sf::Color getOppositeBinColor(const sf::Color & c){
    return c == sf::Color::White ? sf::Color::Black : sf::Color::White;
}

void floodFill(
    sf::Image & im,
    sf::Vector2u pos,
    const sf::Color oC)
{
    const sf::Color bC = getOppositeBinColor(oC);
    const sf::Vector2u imSize = im.getSize();

    // out ob bounds
    if (pos.x < 0 || pos.x > imSize.x - 1 ||
        pos.y < 0 || pos.y > imSize.y - 1) {
        return;
    }

    // add current pos to stack
    std::stack<sf::Vector2u> stack = {};
    stack.push(pos);

    // perform until stack is not empty
    while (!stack.empty()) {
        const sf::Vector2u p = stack.top();
        stack.pop();

        if (p.x < 0 || p.x > imSize.x - 1 ||
            p.y < 0 || p.y > imSize.y - 1) {
            continue;
        }

        if (im.getPixel(p.x, p.y) == oC) {
            im.setPixel(p.x, p.y, bC);

            // search for 8 neighbours
            stack.push({ p.x + 1, p.y }); // R
            stack.push({ p.x - 1, p.y }); // L

            stack.push({ p.x, p.y + 1 }); // U
            stack.push({ p.x, p.y - 1 }); // D
            

            stack.push({ p.x + 1, p.y + 1 }); // RU
            stack.push({ p.x + 1, p.y - 1 }); // RD

            stack.push({ p.x - 1, p.y + 1 }); // LU
            stack.push({ p.x - 1, p.y - 1 }); // LD
        }
    }
}

sf::Image cleanEdges(
    const sf::Image & im,
    const sf::Color & oC)
{
    const sf::Vector2u imSize = im.getSize();    
    const sf::Color bC = getOppositeBinColor(oC);
    sf::Image resIm = im;
    sf::Image newIm = im;
    bool changes = false;

    for (std::uint32_t y = 0; y < imSize.y; ++y) {        
        for (std::uint32_t x = 0; x < imSize.x; ++x) {
            
            // iterate only on borders and objects
            if (y != 0 && 
                y != imSize.y - 1 && 
                x != 0 && x != imSize.x - 1 ||
                resIm.getPixel(x,y) == bC
            ) { continue; }
            
            floodFill(resIm, {x,y}, oC);
        }
    }

    return resIm;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 9

    --- Operations ---
    detect how much girrafe is in image
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

int calcObjs(const sf::Image & im, const sf::Color color)
{
    const sf::Vector2u imSize = im.getSize();
    int sum = 0;

    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {

            if(im.getPixel(x,y) == color)
            {
                sum++;
            }
        }
    }

    return sum;
}

sf::Vector2u centerOfGravityBinImg(const sf::Image & im, const sf::Color c)
{
    const sf::Vector2u imSize = im.getSize();

    int sumXpos = 0;
    int sumYpos = 0;
    int sumXocc = 0;
    int sumYocc = 0;

    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {

            if(im.getPixel(x,y) != c) { continue; }

            sumXocc += 1;
            sumYocc += 1;
            sumXpos += x;
            sumYpos += y;
        }
    }

    return {
        sumXpos/sumXocc,
        sumYpos/sumYocc
    };
}

std::map<std::string, sf::Image> pullImages(
    const std::filesystem::path & path) 
{
    std::map<std::string, sf::Image> ims{};
     
    for (const auto& file : std::filesystem::directory_iterator{path}) {
        sf::Image currIm;
        currIm.loadFromFile(path.string() + file.path().filename().string());
        ims[file.path().filename().string()] = currIm;
    }

    return ims;
}
