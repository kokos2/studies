#pragma once

#include <SFML/Graphics.hpp>
#include <filesystem>
#include <map>
#include <string>
#include <utility>
#include <cstdint>
#include <iostream>
#include <cmath>

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Input Manipulations
    Task 2
        Task 4
            Separating Vector wich moves sprite - separateInputMoveValues
        Task 5
            Separating origin and angle - separateInputOriginAngle
*/
////////////////////////////////////////////////////////////////////////////////////////////////////



std::pair<int, int> separateInputMoveValues(
    const std::string & delimiter, 
    const std::string & op
);

std::pair<std::string, int> separateInputOriginAngle(
    const std::string & delimiter, 
    const std::string & op
);


////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    debugging
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

void printImValuesIfNotBin(const sf::Image & im);
void printImValues(const sf::Image & im);

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 1

    --- Operations ---
    Grayscale
    Zeros Matrix
    Gradient
    Negative
    Binary 
    Arithmetic, Normalization, CutRange
*/
////////////////////////////////////////////////////////////////////////////////////////////////////



// 1 - Gray Image
sf::Image grayScale(const sf::Image & im);


// 2 - Create matrix filled with 0, then make it gradient
sf::Image createZeroMatrix(sf::Vector2u size);
sf::Image createGradientIm(sf::Image zerosMatrix);


// 3 - convert image to negative
sf::Image conToNeg(const sf::Image & im);


// 4 - Bitwise operations
enum class BitOp : const char
{
    And,
    Or,
    Xor
};

sf::Image binary(
    const sf::Image & im0, 
    const sf::Image & im1, 
    const BitOp & op
);


// 5 - Arithmetic, normalization, cutRange
enum class AritOp : const char
{
    Add,
    Sub,
    Mul
};

sf::Image arit(
    const sf::Image & im0, 
    const sf::Image & im1,
    const AritOp & op
);

sf::Image aritCutRange(
    const sf::Image & im0, 
    const sf::Image & im1,
    const AritOp & op
);

#if 0 // changed in task 2
std::uint8_t min_value(const sf::Image & im);
std::uint8_t max_value(const sf::Image & im);
sf::Image norm(const sf::Image & im);
#endif 



////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 2

    --- Operations ---
    Rotate
    Mirror
    Bigger window, sprite center
    Move sprite
    Rotate through origin
    Arit, norm, cuRange RGB
*/
////////////////////////////////////////////////////////////////////////////////////////////////////



// 1 && 5 Rotate
enum class Corner
{
    LU,
    RU,
    LD,
    RD,
    CE
};

Corner convertToEnum(std::string & corner);

sf::Vector2f getOrigin(
    const sf::Image &im, 
    Corner corner
);

float angleRad(float angle);

sf::Vector2u windowSizeRotation(
    const sf::Vector2u size, 
    sf::Vector2f origin, 
    float angle
);

sf::Image rotate(
    const sf::Image & im, 
    sf::Vector2f origin, 
    float angle, 
    sf::Vector2u resultImSize
);


// 2 Mirror
enum class Dimension : const char
{
    Horizontal,
    Vertical
};

sf::Image mirrorReflection(
    const sf::Image & im, 
    const Dimension & op
);


// 3 Bigger window, sprite center
std::pair <int,int> sizeOfBiggerWindowOutsideSprite(
    int xMul, int yMul, 
    int oldWidth, int oldHeight
);

std::pair <int,int> spriteInMiddleOfBiggerWindow(
    int sizeX, int sizeY, 
    int imgWidth, int imgHeight
);

std::pair<int, int> resizeWindowAndMoveSprite(
    int xMul, int yMul, 
    int oldWidth, int oldHeight, 
    sf::Sprite & sprite
);


// 4 Move sprite
void moveSprite(
    sf::Sprite & sprite, 
    float offsetX, 
    float offsetY
);


// 6 arit, norm, cutRange in task 1
sf::Color min_values(const sf::Image & im);
sf::Color max_values(const sf::Image & im);
sf::Image norm(const sf::Image & im);



////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 3

    --- Operations ---
    scaling reproduction method
    Rotate corner did lab2
    Skew angle, basis
    greenscreen
*/
////////////////////////////////////////////////////////////////////////////////////////////////////


// scaling reproduction method

sf::Vector2u windowSizeScale(
    const sf::Image & im, 
    const sf::Vector2f scale
);

sf::Image scaleIm(
    const sf::Image & im,
    const sf::Vector2u size, 
    const sf::Vector2f scale
);

sf::Image fillGaps(
    const sf::Image & im, 
    sf::Vector2u desired_size
);

// Skew angle, basis

enum class Basis : const char
{
    Left,
    Right,
    Down,
    Up
};

sf::Vector2u windowSizeSkew(
    const sf::Vector2u size, 
    Basis basis, 
    float angle
);

sf::Image skew(
    const sf::Image & im, 
    sf::Vector2u resultImSize,
    float angle, 
    Basis basis
);

// greenscreen
std::array <sf::Vector2f, 4> findGreenVertices(
    const sf::Image & im, 
    uint8_t treshold, 
    sf::Color greenVal
);

sf::Image fillGreenScreen(
    const sf::Image & greenScreen, 
    const sf::Image & overlay
);

template<std::size_t N>
sf::Image applyFilter(
    const sf::Image& im, 
    const float (&filter)[N][N]) 
{
    sf::Image resIm;
    resIm.create(im.getSize().x, im.getSize().y);

    const sf::Vector2u imSize = im.getSize();
    const int filterOffset = N / 2; // 7 -> 3

    float filterSum = 0.0f;
    for (const auto& row : filter) {
        for (const auto& val : row) {
            filterSum += val;
        }
    }

    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {
            
            // additional size
            float sumR = 0.0f, sumG = 0.0f, sumB = 0.0f;
            for (int dy = -filterOffset; dy <= filterOffset; ++dy) {
                for (int dx = -filterOffset; dx <= filterOffset; ++dx) {
                    
                    /*
                        imageX/Y
                        if we are beggining with 0,0 we need to take additional
                        pixels from borders based on image offset, could be also clamp,
                        clamps coordinates to be within bounds
                    */
                    int imageX = std::min(std::max(int(x) + dx, 0), int(imSize.x) - 1);
                    int imageY = std::min(std::max(int(y) + dy, 0), int(imSize.y) - 1);

                    sf::Color pixel = im.getPixel(imageX, imageY);
                    
                    /*
                        setting proper filter cords
                    */
                    sumR += pixel.r * filter[dy + filterOffset][dx + filterOffset];
                    sumG += pixel.g * filter[dy + filterOffset][dx + filterOffset];
                    sumB += pixel.b * filter[dy + filterOffset][dx + filterOffset];
                }
            }

            if (filterSum != 0.0f) {
                sumR /= filterSum;
                sumG /= filterSum;
                sumB /= filterSum;
            }

            sumR = std::clamp(sumR, 0.0f, 255.0f);
            sumG = std::clamp(sumG, 0.0f, 255.0f);
            sumB = std::clamp(sumB, 0.0f, 255.0f);
            
            resIm.setPixel(x, y, 
                sf::Color(
                    static_cast<sf::Uint8>(sumR), 
                    static_cast<sf::Uint8>(sumG), 
                    static_cast<sf::Uint8>(sumB)
                )
            );
        }
    }

    return resIm;
}



////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 4

    --- Operations ---
    masks
    noise peper salt
    gaussian noise
        standardDeviation
        getMean
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

sf::Image noisePeperSalt(
    const sf::Image & im, 
    const float frequency
);

sf::Image gaussianNoise(
    const sf::Image &im, 
    const float frequency
);

const std::uint8_t getMean(const sf::Image &im);

const float standardDeviation(const sf::Image &im);

sf::Image additionalBorders(
    const sf::Image & im,
    const std::size_t maskSize
);

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 5 - high pass masks

    --- Operations ---
    any new operations
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 6

    --- Operations ---
    Median masks
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

template<std::size_t N>
sf::Image applyMedian(
    const sf::Image& im, 
    const float (&filter)[N][N])
{
    sf::Image resIm;
    resIm.create(im.getSize().x, im.getSize().y);

    const sf::Vector2u imSize = im.getSize();
    const int filterOffset = N / 2;

    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {
            
            // need vector because of unknow mask weights
            std::vector<std::uint8_t> vec {};
            float pixR = 0.0f;
            for (int dy = -filterOffset; dy <= filterOffset; ++dy) {
                for (int dx = -filterOffset; dx <= filterOffset; ++dx) {
                    
                    // mirror additional borders into original im border
                    int imageX = std::min(std::max(int(x) + dx, 0), int(imSize.x) - 1);
                    int imageY = std::min(std::max(int(y) + dy, 0), int(imSize.y) - 1);

                    // apply occurences of mask weights
                    const int maskTimes = std::abs(filter[dy + filterOffset][dx + filterOffset]);
                    for(int mask = 0; mask < maskTimes; ++mask)
                    {
                        vec.push_back(im.getPixel(imageX, imageY).r);
                    }
                }
            }

            std::sort(vec.begin(), vec.end());

            int vSize = vec.size();
            int vMid = vSize / 2;

            // apply pixel depends if mask is even or odd
            pixR = vSize % 2 == 0 ? (vec[vMid] + vec[vMid-1]) / 2.0f : vec[vMid];

            resIm.setPixel(x, y, 
                sf::Color(
                    static_cast<sf::Uint8>(pixR), 
                    static_cast<sf::Uint8>(pixR), 
                    static_cast<sf::Uint8>(pixR)
                )
            );
        }
    }

    return resIm;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 7

    --- Operations ---
    erosion
    dilation 
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

enum class Morphology {
    erosion,
    dilation
};

// old probably wrong take
template<std::size_t N>
sf::Image morphOp(
    const sf::Image& im, 
    const float (&filter)[N][N],
    const Morphology morph)
{
    sf::Image resIm;
    resIm.create(im.getSize().x, im.getSize().y, sf::Color::White);
    const sf::Vector2u imSize = im.getSize();
    
    const int filterOffset = N / 2;
    // needs threshold because of file compression added some gray values
    const int threshold = 128;

    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {
            
            const sf::Color curPix = im.getPixel(x, y);

            // information if pixel is black of white
            int pixelIntensity = (curPix.r + curPix.g + curPix.b) / 3;

            // check if current pixel is in defined criteria of dilation || erosion
            bool processPixel = 
                (morph == Morphology::dilation) ? 
                (pixelIntensity < threshold) : 
                (pixelIntensity > threshold);

            // ignores pixels wich doesnt meets criteria, then process to next iteration
            if (!processPixel) continue;

            for (int dy = -filterOffset; dy <= filterOffset; ++dy) {
                for (int dx = -filterOffset; dx <= filterOffset; ++dx) {

                    // mirror additional borders into original im border
                    int imX = std::clamp(int(x) + dx, 0, int(imSize.x) - 1);
                    int imY = std::clamp(int(y) + dy, 0, int(imSize.y) - 1);


                    resIm.setPixel(
                        imX, 
                        imY, 
                        sf::Color::Black
                    );
                }
            }
        }
    }

    return resIm;
}

sf::Image morphGradient(
    const sf::Image & im0,
    const sf::Image & im1
);

sf::Image binarize(
    const sf::Image & image, 
    std::uint8_t threshold = 128
);

bool containsTarget(
    const sf::Image & im, 
    const sf::Vector2u pos, 
    const int mask, 
    const sf::Color target
);

// code under shows only space its grown
template<std::size_t N>
sf::Image morphGradientDilatation(
    const sf::Image & im, 
    const float (&filter)[N][N])
{
    sf::Image resIm;
    resIm.create(im.getSize().x, im.getSize().y, sf::Color::White);
    const sf::Vector2u imSize = im.getSize();
    
    const int maskOffset = N / 2; // 3/2 = 1 (lower)

    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {
            
            // skips iteration if we dont met object
            if(im.getPixel(x,y) != sf::Color::Black) continue; 

            // perform operation if found 0 in image in filter
            // so we doesnt fill area if its already filled
            if(!containsTarget(im, sf::Vector2u{x,y}, maskOffset, sf::Color::White)) continue;

            const sf::Color curPix = im.getPixel(x, y);

            for (int dy = -maskOffset; dy <= maskOffset; ++dy) {
                for (int dx = -maskOffset; dx <= maskOffset; ++dx) {

                    // skips iteration if mask word is 0
                    if(filter[dy + maskOffset][dx + maskOffset] == 0) continue;

                    // mirror additional borders into original im border
                    int imX = std::clamp(int(x) + dx, 0, int(imSize.x) - 1);
                    int imY = std::clamp(int(y) + dy, 0, int(imSize.y) - 1);


                    resIm.setPixel(
                        imX, 
                        imY, 
                        sf::Color::Black
                    );
                }
            }
        }
    }

    return resIm;
}

template<std::size_t N>
sf::Image dilation(
    const sf::Image & im, 
    const float (&filter)[N][N])
{
    sf::Image resIm = im;
    const sf::Vector2u imSize = im.getSize();
    
    const int maskOffset = N / 2; // 3/2 = 1 (lower)

    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {
            
            // skips iteration if we dont met object
            if(im.getPixel(x,y) != sf::Color::Black) continue; 

            // perform operation if found white in image in filter
            // so we doesnt fill area if its already filled
            if(!containsTarget(im, sf::Vector2u{x,y}, maskOffset, sf::Color::White)) continue;

            const sf::Color curPix = im.getPixel(x, y);

            for (int dy = -maskOffset; dy <= maskOffset; ++dy) {
                for (int dx = -maskOffset; dx <= maskOffset; ++dx) {

                    // skips iteration if mask word is 0
                    if(filter[dy + maskOffset][dx + maskOffset] == 0) continue;

                    // mirror additional borders into original im border
                    int imX = std::clamp(int(x) + dx, 0, int(imSize.x) - 1);
                    int imY = std::clamp(int(y) + dy, 0, int(imSize.y) - 1);


                    resIm.setPixel(
                        imX, 
                        imY, 
                        sf::Color::Black
                    );
                }
            }
        }
    }

    return resIm;
}

template<std::size_t N>
sf::Image erosion(
    const sf::Image & im, 
    const float (&filter)[N][N])
{
    sf::Image resIm = im;
    const sf::Vector2u imSize = im.getSize();
    
    const int maskOffset = N / 2; // 3/2 = 1 (lower)

    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {
            
            // skips iteration if we hit object
            if(im.getPixel(x,y) != sf::Color::Black) continue; 

            // perform operation if found Black in image in filter
            // so we doesnt erase area if its bigger than mask
            if(!containsTarget(im, sf::Vector2u{x,y}, maskOffset, sf::Color::White)) continue;

            const sf::Color curPix = im.getPixel(x, y);

            for (int dy = -maskOffset; dy <= maskOffset; ++dy) {
                for (int dx = -maskOffset; dx <= maskOffset; ++dx) {

                    // skips iteration if mask word is 0
                    if(filter[dy + maskOffset][dx + maskOffset] == 0) continue;

                    // mirror additional borders into original im border
                    int imX = std::clamp(int(x) + dx, 0, int(imSize.x) - 1);
                    int imY = std::clamp(int(y) + dy, 0, int(imSize.y) - 1);


                    resIm.setPixel(
                        imX, 
                        imY, 
                        sf::Color::White
                    );
                }
            }
        }
    }

    return resIm;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 8

    --- Operations ---
    conditional erosion
    thinning
    thickening
    detect end points
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

sf::Color getOppositeBinColor(const sf::Color & c);

template<std::size_t N, std::size_t M>
bool containsGroupMask(
    const sf::Image & im, 
    const sf::Vector2u & pos,
    const sf::Color & oC,
    const int (&filter)[M][N][N])
{
    const sf::Vector2u imSize = im.getSize();
    const uint8_t maskOffset = N/2; // 7 -> 3
    constexpr int x = 2; // unwanted mask word
    const sf::Color bC = getOppositeBinColor(oC);

    // all masks
    for (int dz = 0; dz < M; ++dz) {
        int sim = 0;
        int maskWords = 0;

        // all words
        for (int dy = -maskOffset; dy <= maskOffset; ++dy) {
            for (int dx = -maskOffset; dx <= maskOffset; ++dx) {
                
                // skips unwanted mask word
                if(filter[dz][dy + maskOffset][dx + maskOffset] == x) continue;

                // count how many wanted words in mask we have
                maskWords++;

                // check if mask is 0 black or 1 white    
                const sf::Color maskWordColor = 
                    filter[dz][dy + maskOffset][dx + maskOffset] ? oC : bC;

                // clamp into bounds
                int imX = std::clamp(int(pos.x) + dx, 0, int(imSize.x) - 1);
                int imY = std::clamp(int(pos.y) + dy, 0, int(imSize.y) - 1);

                // check if we have situation from current mask
                if(im.getPixel(imX,imY) == maskWordColor) sim++;
            }
        }
        
        // if we have situation from mask return true
        if(sim == maskWords) return true;
    }

    return false;
}

template<std::size_t N, std::size_t M>
sf::Image hitAndMissTransform(
    const sf::Image & im, 
    const sf::Color & oC,
    const int (&filter)[M][N][N])
{
    const sf::Color bC = getOppositeBinColor(oC); // background Color
    const sf::Vector2u imSize = im.getSize();
    sf::Image resIm;
    resIm.create(im.getSize().x, im.getSize().y, bC);
    
    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {
            
            // skips if we are vibing on background
            if(im.getPixel(x,y) == bC) continue; 

            // skips if we dont met all masks circumstances
            if(!containsGroupMask(im,{x,y},oC,filter)) { continue; }
            resIm.setPixel(x,y,oC);
        }
    }

    return resIm;
}

template<std::size_t N, std::size_t M>
sf::Image thinningTransform(
    const sf::Image & im, 
    const sf::Color & oC,
    const int (&filter)[M][N][N])
{
    const sf::Color bC = getOppositeBinColor(oC);
    const sf::Vector2u imSize = im.getSize();
    sf::Image resIm = im; 
    sf::Image newIm = resIm; 
    bool changes = false;
    
    do {
        changes = false;

        for (std::uint32_t y = 0; y < imSize.y; ++y) {
            for (std::uint32_t x = 0; x < imSize.x; ++x) {
                if(resIm.getPixel(x,y) == bC) continue; 

                if(!containsGroupMask(newIm,{x,y},oC,filter)) { continue; }
                newIm.setPixel(x,y,bC);
                changes = true;
            }
        }

        resIm = newIm;

    }while(changes);

    return resIm;
}



int calcObjs(
    const sf::Image & im, 
    const sf::Color color
);

template<std::size_t N>
sf::Image erosionConditional(
    const sf::Image & im, 
    const sf::Color & oC,
    const int & neighbours,
    const float (&filter)[N][N])
{
    const sf::Vector2u imSize = im.getSize();    
    const sf::Color bC = getOppositeBinColor(oC);
    const int maskOffset = N / 2; // 3/2 = 1 (lower)

    sf::Image resIm = im; 
    sf::Image newIm = resIm; 
    int perim0 = 0;
    int perim1 = 0;

    do {
        for (std::uint32_t y = 0; y < imSize.y; ++y) {
            for (std::uint32_t x = 0; x < imSize.x; ++x) {

                if (newIm.getPixel(x, y) == bC) continue; 
                if (!containsTarget(newIm, {x, y}, maskOffset, bC)) continue;

                int objSum = 0;
                for (int dy = -maskOffset; dy <= maskOffset; ++dy) {
                    for (int dx = -maskOffset; dx <= maskOffset; ++dx) {

                        int imX = std::clamp(int(x) + dx, 0, int(imSize.x) - 1);
                        int imY = std::clamp(int(y) + dy, 0, int(imSize.y) - 1);

                        // Check how many black pixels we have in the mask
                        if (newIm.getPixel(imX, imY) == oC) objSum++;
                    }
                }
                if (objSum > neighbours) resIm.setPixel(x, y, bC);
                }
            }


        /* could also check if any new pixels were changed 
            instead of calculating perim
        */
        perim0 = calcObjs(newIm, oC);
        perim1 = calcObjs(resIm, oC);
        newIm = resIm;

    } while (perim0 != perim1);

    return newIm;
}

template<std::size_t N, std::size_t M>
sf::Image clearInsides(
    const sf::Image & im, 
    const sf::Color & oC,
    const int (&filter)[M][N][N])
{
    const sf::Color bC = getOppositeBinColor(oC);
    const sf::Vector2u imSize = im.getSize();
    sf::Image resIm = im; 

    for (std::uint32_t y = 0; y < imSize.y; ++y) {
        for (std::uint32_t x = 0; x < imSize.x; ++x) {
            if(im.getPixel(x,y) == bC) continue; 

            if(!containsGroupMask(im,{x,y},oC,filter)) { continue; }
            resIm.setPixel(x,y,bC);
        }
    }

    return resIm;
}

template<std::size_t N>
sf::Image thickeningTransform(
    const sf::Image & im, 
    const sf::Color & oC,
    const float (&filter)[N][N],
    const int & neighbours = 3)
{
    const sf::Vector2u imSize = im.getSize();    
    const sf::Color bC = getOppositeBinColor(oC);
    const int maskOffset = N / 2; // 3/2 = 1 (lower)

    sf::Image resIm = im; 
    sf::Image newIm = resIm; 
    int perim0 = 0;
    int perim1 = 0;

    do {
        for (std::uint32_t y = 0; y < imSize.y; ++y) {
            for (std::uint32_t x = 0; x < imSize.x; ++x) {

                if (newIm.getPixel(x, y) == oC) continue; 
                if (!containsTarget(newIm, {x, y}, maskOffset, bC)) continue;

                int objSum = 0;
                for (int dy = -maskOffset; dy <= maskOffset; ++dy) {
                    for (int dx = -maskOffset; dx <= maskOffset; ++dx) {

                        int imX = std::clamp(int(x) + dx, 0, int(imSize.x) - 1);
                        int imY = std::clamp(int(y) + dy, 0, int(imSize.y) - 1);

                        if (newIm.getPixel(imX, imY) == oC) objSum++;
                    }
                }

                if (objSum > neighbours) {
                    resIm.setPixel(x, y, oC);
                }
            }
        }

        perim0 = calcObjs(newIm, oC);
        perim1 = calcObjs(resIm, oC);
        newIm = resIm;

    } while (perim0 != perim1);

    return newIm;
}


sf::Image cleanEdges(
    const sf::Image & im,
    const sf::Color & oC
);

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
    Task 9

    --- Operations ---
    detect how much girrafe is in image
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

int calcObjs(
    const sf::Image & im, 
    const sf::Color color
);

std::map<std::string, sf::Image> pullImages(
    const std::filesystem::path & path
);

sf::Vector2u centerOfGravityBinImg(
    const sf::Image & im, 
    const sf::Color c
);
