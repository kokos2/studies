## Technology
``` 
By default on this subject everyone uses MATLAB
but lectuter told that you can use whatever you wants
so im using C++ with SFML
```

## functions
``` 
Library for every task
```

## Zestaw nr 1
``` 
    --- Operations ---
    Grayscale
    Zeros Matrix
    Gradient
    Negative
    Binary 
    Arithmetic, Normalization, CutRange
```

## Zestaw nr 2
``` 
    --- Operations ---
    Rotate
    Mirror
    Bigger window, sprite center
    Move sprite
    Rotate through origin
    Arit, norm, cuRange RGB
```

## Zestaw nr 3
``` 
    --- Operations ---
    scaling reproduction method
    Rotate corner did lab2
    Skew angle, basis
    greenscreen
```

## Zestaw nr 4
``` 
    --- Operations ---
    masks
    noise peper salt
    gaussian noise
        standardDeviation
        getMean
```

## Zestaw nr 5
``` 
    --- Operations ---
    masks from 4
```
## Zestaw nr 6
``` 
    --- Operations ---
    Median masks
```

## Zestaw nr 7
``` 
    --- Operations ---
    erosion
    dilation 
    morphologic gradient
    binarize
```

## Zestaw nr 8
``` 
    --- Operations ---
    conditional erosion
    thinning
    thickening
    detect end points - directional
    clear shores
    clear insides
```

## Zestaw nr 9
``` 
    --- Operations ---
    pattern recognition
    by entering 3 simmilar imnages
    you can compare with other images
    to test if this image is simmilar
    to ones wich you entered
```
