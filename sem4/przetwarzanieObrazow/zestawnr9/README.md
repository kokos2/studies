# Lab 9

## Building
``` sh
$ git clone --recursive https://gitlab.com/kokos2/studies.git
$ mkdir build && mkdir images && cd build
$ cmake ..
$ cmake --build .
```
