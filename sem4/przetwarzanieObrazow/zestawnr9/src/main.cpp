#include <functions.hpp>
#include <array>
#include <map>
#include <iostream>
#include <filters.hpp>

void menu()
{
    std::cout << "Usage: ./zestawnr9 <task> <image> <imNr>" << std::endl;
    const std::string tab = "\t";
    const std::string tabBig = "\t\t";
    using namespace std;
    cout << endl << "Operations :" << endl;
    cout << "\"0\", \"1\", \"2\", \"3\", \"4\", \"5\", \"6\", \"bb\", \"im\", \"ims\"\n" << endl;
    cout << "0 - perimeter" << endl;
    cout << "1 - surface area" << endl;
    cout << "2 - width" << endl;
    cout << "3 - height" << endl;
    cout << "4 - geometric center" << endl;
    cout << "5 - gravity center" << endl;
    cout << "6 - get average properties of chosen group of images" << endl;
    cout << "bb - show bounding box" << endl;
    cout << "im - chose image to calculate coefficients and print target image" << endl;
    cout << "ims - chose image to calculate coefficients and print rest images" << endl;
    cout << endl << "Available images :" << endl;
    cout << tab + "girrafe" << endl;
    cout << tab + "barca" << endl;
    cout << tab + "chmura" << endl;
    cout << tab + "dlon" << endl;
    cout << tab + "gwiazda" << endl;
    cout << tab + "helm" << endl;
    cout << tab + "human / humanCon" << endl;
    cout << tab + "klucz" << endl;
    cout << tab + "miecz" << endl;
    cout << tab + "serce" << endl;
    cout << tab + "cat / catCon" << endl;
    cout << endl;
}

class MoreWindows {
public:
    MoreWindows() = default;

    MoreWindows(
        const std::string & title, 
        const sf::Sprite & sprite, 
        const sf::Texture & texture, 
        sf::Vector2u size) 
        : savePath(title), sprite(sprite), texture(texture) 
    {
        this->sprite.setTexture(this->texture);
        window.create(sf::VideoMode(size.x, size.y), title, sf::Style::None);
        renderTexture.create(size.x, size.y);
    }

    void screenShot() const 
    {
        renderTexture.getTexture().copyToImage().saveToFile(savePath);
    }

    void run() {

        if (!window.isOpen()) { return; }

        sf::Event event;
        while (window.pollEvent(event)) 
        {
            if (event.type == sf::Event::Closed) { window.close(); }
        }

        renderTexture.clear(sf::Color::Black);
        renderTexture.draw(sprite);
        renderTexture.display();

        window.clear();
        sf::Sprite spr (renderTexture.getTexture());
        window.draw(spr);
        window.display();
    }

    bool isOpen() const {
        return window.isOpen();
    }

    sf::RenderWindow& getWindow() {
        return window;
    }
    
private:
    sf::Texture texture;
    const std::string savePath; 
    sf::RenderWindow window;
    sf::RenderTexture renderTexture;
    sf::Sprite sprite;
};

class Properties
{
public:
    double perim;
    double area;
    double height;
    double width;
    sf::Vector2u centroid;
    sf::Vector2u gravityCenter;

    Properties(double p, double a, double h, double w, sf::Vector2u c, sf::Vector2u gC) 
        : perim(p), area(a), height(h), width(w), centroid(c), gravityCenter(gC) {}
};

class Application 
{    
    void pushWindow(const std::string & title, const sf::Image & im, sf::Vector2u size) 
    {
        sf::Texture texture;
        texture.loadFromImage(im);
        sf::Sprite sprite;
        sprite.setTexture(texture);
    
        new (&windows[count++]) MoreWindows { title, sprite, texture, size };

        windows[count - 1].getWindow().setPosition(
            sf::Vector2i { 
                static_cast<int>(960u - (im.getSize().x / 2u)),
                static_cast<int>(540u - (im.getSize().y / 2u))
            }
        );  
    }

    void pushWindow(const std::string & title, const sf::Sprite & sprite, sf::Vector2u size) 
    {
        new (&windows[count++]) MoreWindows { title, sprite, *sprite.getTexture(), size };
        
    }
    
    template<std::size_t N>
    Properties storeProperties(
        const std::array<sf::Image, 3>& images, 
        const sf::Color& oC, 
        const float (&mE)[N][N], 
        const float (&mD)[N][N])
    {
        double totalPerim = 0;
        double totalSurfArea = 0;
        double totalHeight = 0;
        double totalWidth = 0;
        sf::Vector2u totalCentroid = {0, 0};
        sf::Vector2u totalGravCent = {0, 0};

        for (const auto& img : images)
        {
            std::array<sf::Vector2f, 4> vertices = findGreenVertices(img, 128, oC);

            sf::Image mG = morphGradient(
                erosion( conToNeg(img), mE), 
                dilation(conToNeg(img), mD)
            );

            const double perim = calcObjs(mG, oC);
            totalPerim += perim;

            const double surfArea = calcObjs(img, oC);
            totalSurfArea += surfArea;

            const double height = vertices[2].y - vertices[0].y;
            totalHeight += height;

            const double width = vertices[2].x - vertices[0].x;
            totalWidth += width;

            const sf::Vector2u centroid = {width / 2, height / 2};
            totalCentroid.x += centroid.x;
            totalCentroid.y += centroid.y;

            const sf::Vector2u gravCent = centerOfGravityBinImg(img, oC);
            totalGravCent.x += gravCent.x - vertices[0].x;
            totalGravCent.y += gravCent.y - vertices[0].y;
        }

        // average values of all images
        const double s = images.size();
        const double perimeter   = totalPerim / s;
        const double surfaceArea = totalSurfArea / s;
        const double height      = totalHeight / s;
        const double width       = totalWidth / s;
        const sf::Vector2u centroid = {
            totalCentroid.x / s, 
            totalCentroid.y / s
        };
        const sf::Vector2u gravCent = {
            totalGravCent.x / s, 
            totalGravCent.y / s
        };

        return Properties(perimeter, surfaceArea, height, width, centroid, gravCent);
    }

    double calDiff(
        const Properties  & pG,
        const Properties  & pT)
    {
        // param
        #define DIFF(p1, p2)      \
            (((p1) + (p2)) != 0 ? \
                (std::abs((p1) - (p2)) / (((p1) + (p2)) / 2.0) * 100) : 0)

        #define PERCENT_DIFFERENCE_COORDINATES(x1, y1, x2, y2) \
            ((DIFF(x1, x2) + DIFF(y1, y2)) / 2)
        
        #define NORMALIZED_CENTROID(x1, y1, x2, y2, aR1, aR2) \
            ((DIFF((x1) / (aR1), (x2) / (aR2)) + DIFF((y1) / (aR1), (y2) / (aR2))) / 2)

        double perToArea = DIFF(pG.perim / pG.area, pT.perim / pT.area);

        double aRS = pG.width / pG.height;
        double aRT = pT.width / pT.height;
        
        double normCentr = NORMALIZED_CENTROID(
            static_cast<double>(pG.centroid.x), 
            static_cast<double>(pG.centroid.y), 
            static_cast<double>(pT.centroid.x), 
            static_cast<double>(pT.centroid.y),
            static_cast<double>(aRS),
            static_cast<double>(aRT)
        );

        double gravity = PERCENT_DIFFERENCE_COORDINATES(
            static_cast<double>(pG.gravityCenter.x), 
            static_cast<double>(pG.gravityCenter.y), 
            static_cast<double>(pT.gravityCenter.x), 
            static_cast<double>(pT.gravityCenter.y)
        );

        return (gravity + perToArea + normCentr)/3;
    }

    void printPropertiesAndInfo(
        const std::string & group, 
        const std::string & target,
        const Properties  & pG,
        const Properties  & pT,
        const double & sim)
    {
        #if 0
        {
            using namespace std;
            cout << endl;
            cout << "Average properties of images showing : " + group << endl;
            cout << "Perimeter : " << pG.perim << endl;
            cout << "Area : "      << pG.area << endl;
            cout << "Height : "    << pG.height << endl;
            cout << "Width : "     << pG.width << endl;
            cout << "Gravity center : (in bounding box) : x = " << pG.gravityCenter.x <<  
                                                        " y = " << pG.gravityCenter.y 
            << endl;

            cout << "Centroid (in bounding box) : x = " << pG.centroid.x <<  
                                                " y = " << pG.centroid.y 
            << "\n" << endl;
            
            cout << endl;
            cout << "Properties of image showing : " + target << endl;
            cout << "Perimeter : " << pT.perim << endl;
            cout << "Area : "      << pT.area << endl;
            cout << "Height : "    << pT.height << endl;
            cout << "Width : "     << pT.width << endl;
            cout << "Gravity center : (in bounding box) : x = " << pT.gravityCenter.x <<  
                                                        " y = " << pT.gravityCenter.y 
            << endl;

            cout << "Centroid (in bounding box) : x = " << pT.centroid.x <<  
                                                " y = " << pT.centroid.y 
            << "\n" << endl;
        }
        #endif

        std::cout 
            << std::setw(16) << std::left << target << "- " 
            << std::setw(8)  << std::left << sim << "%. ";

        if(sim < 20)
        {
            std::cout << "it's " + group << " !!!" << std::endl;
        }else{
            std::cout << "!" + group << std::endl;
        }
    }

public:

    void run(const std::string & task, const std::string & imName, const std::string & imNr)
    {
        /*
            if you want more images to examine, change in store properties, task 6, im, ims
        */
        sf::Image im0, im1, im2;
        const std::string currentImage = imName;
        const std::string path = "../images/all_images/";
        const std::string fileT = ".png";
        im0.loadFromFile(path + currentImage + "0" + fileT);
        im1.loadFromFile(path + currentImage + "1" + fileT);
        im2.loadFromFile(path + currentImage + "2" + fileT);
        
        int imgHeight = (im0.getSize().y);
        int imgWidth  = (im0.getSize().x);
        
        sf::Texture resultTexture;
        std::pair<int, int> tempSize = std::make_pair(imgWidth,imgHeight);

        im0 = binarize(im0);
        im1 = binarize(im1);
        im2 = binarize(im2);
        
        auto& mD = mask3;
        auto& mE = mask3E;

        const sf::Color oC = sf::Color::White; // object color
            
        sf::Image img;
        if(task != "im")
        {
            img.loadFromFile(path + currentImage + imNr + fileT);
        } else { // refactor
            img.loadFromFile(path + imNr + fileT);
        }
        sf::Image im = binarize(img, 230); // teams compression

        // perimeter 
        if (task == "0")
        {
            sf::Image mG = morphGradient(
                erosion(im, mE), 
                dilation(im, mD)
            );

            pushWindow("../images/0/perimeter.png", mG, mG.getSize());
            
            std::cout << "\nImage : " << imName << imNr << std::endl <<
                            "\tperimeter : " << calcObjs(mG, oC) << std::endl;
        }
        // surface area
        else if(task == "1")
        {
            pushWindow("../images/1/surfaca_area.png", im, im.getSize());
            
            std::cout << "\nImage : " << imName << imNr << std::endl << 
                            "\tSurface area : " << calcObjs(im, oC) << std::endl;
        }
        // height
        else if(task == "2")
        {       
            std::array <sf::Vector2f, 4> v = findGreenVertices(im, 44, oC);
            const int height = v[2].y - v[0].y;

            const int middle = (v[0].x + v[2].x)/2;
            // print line for better view of vertices
            for (unsigned int y = v[0].y; y <= v[2].y; ++y)
            {
                im.setPixel(middle, y, sf::Color::Red);
            }

            pushWindow("../images/2/height.png", im, im.getSize());
            
            std::cout << "\nImage : " << imName << imNr << std::endl <<
                            "\tHeight : " << height << std::endl;
        }
        // width
        else if(task == "3")
        {
            std::array <sf::Vector2f, 4> v = findGreenVertices(im, 44, oC);
            const int width = v[2].x - v[0].x;

            const int middle = (v[0].y + v[2].y)/2;
            // print line for better view of vertices
            for (unsigned int x = v[0].x; x <= v[2].x; ++x)
            {
                im.setPixel(x, middle, sf::Color::Red);
            }

            pushWindow("../images/3/width.png", im, im.getSize());
            
            std::cout << "\nImage : " << imName << imNr << std::endl <<
                            "\tWidth : " << width << std::endl;
        }
        // centroid
        else if(task == "4")
        {
            std::array <sf::Vector2f, 4> v = findGreenVertices(im, 44, oC);
            const int width  = v[2].x - v[0].x;
            const int height = v[2].y - v[0].y;
            const sf::Vector2u centroid = {
                width/2,
                height/2
            };
            
            im.setPixel(centroid.x + v[0].x, centroid.y + v[0].y, sf::Color::Red);
            pushWindow("../images/4/centroid.png", im, im.getSize());
            
            std::cout << "\nImage : " << imName << imNr << std::endl <<
                            "\tCentroid (in bounding box) : x = " << centroid.x <<  
                                                          " y = " << centroid.y 
            << std::endl;

            std::cout <<    "\tCentroid (on screen) : x = " << centroid.x + v[0].x <<  
                                                    " y = " << centroid.y + v[0].y 
            << std::endl;

        }
        // center of gravity        
        else if(task == "5")
        {            
            std::array <sf::Vector2f, 4> v = findGreenVertices(im, 44, oC);
            const sf::Vector2u cG = centerOfGravityBinImg(im, oC);
            
            im.setPixel(cG.x, cG.y, sf::Color::Red);
            pushWindow("../images/5/centerGravity.png", im, im.getSize());

            std::cout << "\nImage : " << imName << imNr << std::endl <<
                "\tCenter of gravity (in bounding box) : x = " << cG.x - v[0].x <<  
                                                       " y = " << cG.y - v[0].y << std::endl;
            std::cout << 
                "\tCenter of gravity (on screen) : x = " << cG.x <<  
                                                 " y = " << cG.y << std::endl;
        }
        // bounding box
        else if(task == "bb")
        {
            std::array <sf::Vector2f, 4> v = findGreenVertices(im, 44, oC);

            // draw bounding box 
            for (unsigned int x = v[0].x; x <= v[2].x; ++x)
            {
                im.setPixel(x, v[0].y, sf::Color::Red);
                im.setPixel(x, v[2].y, sf::Color::Red);
            }

            for (unsigned int y = v[0].y; y <= v[2].y; ++y)
            {
                im.setPixel(v[0].x, y, sf::Color::Red);
                im.setPixel(v[2].x, y, sf::Color::Red);
            }

            pushWindow("../images/bb/boundingBox.png", im, im.getSize());
        }
        // show properties
        else if(task == "6")
        {
            std::array<sf::Image, 3> images = {im0, im1, im2};

            bool showIms = false;
            #if showIms
                pushWindow("../images/6/" + currentImage + "0.png", im0, im0.getSize());
                pushWindow("../images/6/" + currentImage + "1.png", im1, im0.getSize());
                pushWindow("../images/6/" + currentImage + "2.png", im2, im0.getSize());
            #endif

            Properties props = storeProperties(images, oC, mE, mD);

            {
                using namespace std;
                cout << endl;
                cout << "Average properties of images showing : " + currentImage << endl;
                cout << "Perimeter : " << props.perim << endl;
                cout << "Area : "      << props.area << endl;
                cout << "Height : "    << props.height << endl;
                cout << "Width : "     << props.width << endl;
                cout << "Gravity center : (in bounding box) : x = " << props.gravityCenter.x <<  
                                                            " y = " << props.gravityCenter.y 
                << endl;

                cout << "Centroid (in bounding box) : x = " << props.centroid.x <<  
                                                    " y = " << props.centroid.y 
                << "\n" << endl;
            }
        }

        // compare properties of images with image
        else if(task == "im")
        {
            // input is ./zestaw9 image <group to set properteis> <target image>
            std::array<sf::Image, 3> images = {im0, im1, im2};
            Properties propsGroup = storeProperties(images, oC, mE, mD);

            std::array<sf::Image, 3> imgComp = {im,im,im};
            Properties propsComp = storeProperties(imgComp, oC, mE, mD);

            std::string fileName = imNr;
            double diff = calDiff(propsGroup, propsComp);
            printPropertiesAndInfo(imName,imNr,propsGroup,propsComp,diff);
            pushWindow("../images/compare/imTarget.png", im, im.getSize());
        }
        // compare current properties image with everything
        else if(task == "ims")
        {            
            // input is ./zestaw9 image <group to set properteis> <doesnt matter>
            std::array<sf::Image, 3> images = {im0, im1, im2};
            Properties propsGroup = storeProperties(images, oC, mE, mD);
            
            std::map<std::string, sf::Image> ims = pullImages(path);
            
            constexpr int tS = 39;
            std::cout << std::string(tS,'=') << "\n im compared | group : " + imName + " | result" 
            << std::endl;
            
            for (const auto& [fileName, image] : ims) {

                std::array<sf::Image, 3> imge = {
                    binarize(image), 
                    binarize(image), 
                    binarize(image)
                };

                Properties propsComp = storeProperties(imge, oC, mE, mD);
                double diff = calDiff(propsGroup, propsComp);
                printPropertiesAndInfo(imName,fileName,propsGroup,propsComp,diff);
            }
            std::cout << std::string(tS,'=') << std::endl;
        }
        else
        {
            std::cout << "Wrong task" << std::endl;
        }

        while (true) 
        {
            std::size_t open = 0u;
            
            for (std::size_t i = 0u; i < count; i++) 
            {
                if (windows[i].isOpen()) { open++; }
            }

            if (open == 0) {
                break;
            }

            for (std::size_t i = 0u; i < count; i++) {
                windows[i].run();
            }
        }

        for (std::size_t i = 0u; i < count; i++) 
        {
            windows[i].screenShot();
        }
    }

private:
    std::array<MoreWindows, 40> windows = {};
    std::size_t count = 0;
};

int main (int argc, char* argv[])
{

    if (argc != 4)
    {
        menu();
        return 1;
    }

    std::string task(argv[1]);
    std::string imName(argv[2]);
    std::string imNr(argv[3]);
    Application app;
    app.run(task, imName, imNr);

    return 0;
}

// TODO
/*
    compare simmilarities
    sort compare images

    comparing only one image
*/
