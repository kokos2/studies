#include <functions.hpp>
#include <array>
#include <iostream>
#include <morphology.hpp>
#include <morphologyTest.hpp>
#include <filters.hpp>

void menu()
{
    const std::string tab = "\t";
    const std::string tabBig = "\t\t";
    using namespace std;
    cout << endl << "Operations :" << endl;
    cout << "\"1\", \"2\", \"3\", \"4\", \"5\", \"6\"\n" << endl;
    cout << "1 - erosion cond" << endl;
    cout << "2 - thinning TODO" << endl;
    cout << "3 - thickening" << endl;
    cout << "4 - end points options : \"all\", \"fun\"" << endl;
    cout << "5 - clean edges TODO" << endl;
    cout << "6 - clean insides" << endl;
    cout << "7 - thinning tests" << endl;
}

class MoreWindows {
public:
    MoreWindows() = default;

    MoreWindows(
        const std::string & title, 
        const sf::Sprite & sprite, 
        const sf::Texture & texture, 
        sf::Vector2u size) 
        : savePath(title), sprite(sprite), texture(texture) 
    {
        this->sprite.setTexture(this->texture);
        window.create(sf::VideoMode(size.x, size.y), title, sf::Style::None);
        renderTexture.create(size.x, size.y);
    }

    void screenShot() const 
    {
        renderTexture.getTexture().copyToImage().saveToFile(savePath);
    }

    void run() {

        if (!window.isOpen()) { return; }

        sf::Event event;
        while (window.pollEvent(event)) 
        {
            if (event.type == sf::Event::Closed) { window.close(); }
        }

        renderTexture.clear(sf::Color::Black);
        renderTexture.draw(sprite);
        renderTexture.display();

        window.clear();
        sf::Sprite spr (renderTexture.getTexture());
        window.draw(spr);
        window.display();
    }

    bool isOpen() const {
        return window.isOpen();
    }
    
    sf::RenderWindow& getWindow() {
        return window;
    }
private:
    sf::Texture texture;
    const std::string savePath; 
    sf::RenderWindow window;
    sf::RenderTexture renderTexture;
    sf::Sprite sprite;
};

class Application 
{    
    void pushWindow(const std::string & title, const sf::Image & im, sf::Vector2u size) 
    {
        sf::Texture texture;
        texture.loadFromImage(im);
        sf::Sprite sprite;
        sprite.setTexture(texture);
        
        new (&windows[count++]) MoreWindows { title, sprite, texture, size };
        
        windows[count - 1].getWindow().setPosition(
            sf::Vector2i { 
                static_cast<int>(960u - (im.getSize().x / 2u)),
                static_cast<int>(540u - (im.getSize().y / 2u))
            }
        );    
    }
    
    void pushWindow(const std::string & title, const sf::Sprite & sprite, sf::Vector2u size) 
    {
        new (&windows[count++]) MoreWindows { title, sprite, *sprite.getTexture(), size };
    }
    
public:

    void run(const std::string & task, const std::string & operation)
    {
        sf::Image im0, im1, im2;
        im0.loadFromFile("../../images/choina.png");
        im1.loadFromFile("../../images/hehe.jpg");
        im2.loadFromFile("../../images/binLab9/klucz2.png");
        
        const int imgHeight = (im0.getSize().y);
        const int imgWidth  = (im0.getSize().x);
        
        const sf::Texture resultTexture;
        const std::pair<int, int> tempSize = std::make_pair(imgWidth,imgHeight);

        im0 = binarize(im0, 2);
        im1 = binarize(conToNeg(im1));
        im2 = binarize(im2);

        const auto& mE = mask3E;
        const sf::Color oC = sf::Color::White;
        const sf::Image im = im0;

        // conditional erosion - done
        if(task == "1") 
        {
            sf::Image e = erosionConditional(im, oC, 2, mE);
            pushWindow("../images/1/eros.png", e, e.getSize());
            pushWindow("../images/1/comparing.png", im, im.getSize());
        }
        // thinning - TODO
        else if(task == "2")
        {
            auto& masks = thinning::m;
            sf::Image t = thinningTransform(im, oC, masks);
            pushWindow("../images/2/thinning.png", t, t.getSize());
            pushWindow("../images/2/comparing.png", im, im.getSize());
        }
        // thickening - kinda done 
        else if(task == "3")
        {
            sf::Image t = thickeningTransform(im, oC, mE);
            pushWindow("../images/3/thickening.png", t, t.getSize());
            pushWindow("../images/3/comparing.png", im, im.getSize());
        }
        // detect end points - hit and miss - done
        else if(task == "4")
        {       
            auto& masks = hit_and_miss::m;
            sf::Image hitAndMiss = hitAndMissTransform(im, oC, masks);

            // directions            
            auto& dR = hit_and_miss_right::m;
            auto& dL = hit_and_miss_left::m;
            auto& dD = hit_and_miss_down::m;
            auto& dU = hit_and_miss_up::m;

            sf::Image right = hitAndMissTransform(im, oC, dR);
            sf::Image left  = hitAndMissTransform(im, oC, dL);
            sf::Image down  = hitAndMissTransform(im, oC, dD);
            sf::Image up    = hitAndMissTransform(im, oC, dU);

            // for kids
            auto& masksTest = hit_and_miss_test::m;
            sf::Image hitAndMissTest = hitAndMissTransform(im, oC, masksTest);

            if(operation == "all")
            {
                pushWindow("../images/4/hitAndMiss.png", hitAndMiss, hitAndMiss.getSize());
                pushWindow("../images/4/comparing.png", im, hitAndMiss.getSize());
                pushWindow("../images/4/right.png", right, right.getSize());
                pushWindow("../images/4/left.png", left, left.getSize());
                pushWindow("../images/4/down.png", down, down.getSize());
                pushWindow("../images/4/up.png", up, up.getSize());
            }
            else if(operation == "fun") // additional
            {
                pushWindow("../images/4/hitAndMissTest.png", hitAndMissTest, hitAndMiss.getSize());
                pushWindow("../images/4/hitAndMiss.png", hitAndMiss, hitAndMiss.getSize());
                pushWindow("../images/4/comparing.png", im, hitAndMiss.getSize());
            }
        }
        // clean edges - TODO
        else if(task == "5")
        {
            sf::Image fig;
            fig.loadFromFile("../../images/figures.png");

            sf::Image cE = cleanEdges(fig, oC);
            pushWindow("../images/5/cleanEdges.png", cE, cE.getSize());
            pushWindow("../images/5/comparing.png", fig, fig.getSize());
        }
        // clear insides - additional done
        else if(task == "6")
        {
            auto& m = clear_insides::m;
            sf::Image cI = clearInsides(im, oC, m);
            pushWindow("../images/6/condEros.png", cI, cI.getSize());
            pushWindow("../images/6/comparing.png", im, im.getSize());
        }        
        else if(task == "7") // test
        {
        
        }
        else
        {   
            pushWindow("../images/default.jpg", im0, im0.getSize());
            std::cout << "default image shown" << std::endl;
        }

        while (true) 
        {
            std::size_t open = 0u;
            
            for (std::size_t i = 0u; i < count; i++) 
            {
                if (windows[i].isOpen()) { open++; }
            }

            if (open == 0) {
                break;
            }

            for (std::size_t i = 0u; i < count; i++) {
                windows[i].run();
            }
        }

        for (std::size_t i = 0u; i < count; i++) 
        {
            windows[i].screenShot();
        }
    }

private:
    std::array<MoreWindows, 10> windows = {};
    std::size_t count = 0;
};

int main (int argc, char* argv[])
{
    menu();

    if (argc != 3)
    {
        std::cerr << "Usage: ./zestawnr <task> <operation/all>" << std::endl;
        return 1;
    }

    std::string task(argv[1]);
    std::string operation(argv[2]);
    Application app;
    app.run(task, operation);

    return 0;
}
