#include <functions.hpp>
#include <iostream>
#include <array>

void menu()
{
    const std::string tab = "        ";
    const std::string tabBig = "                     ";
    std::cout << "Usage : ./zestawnr2 <task> <operation/all>" << std::endl;
    std::cout << "Tasks : " << std::endl;
    std::cout << tab << "1<task>" << std::endl;
    std::cout << tab << "flip operations" << std::endl;
    std::cout << tab << "operations : " << std::endl;
    std::cout << tabBig << "90<operation>" << std::endl;
    std::cout << tabBig << "180" << std::endl;
    std::cout << tabBig << "270" << std::endl;
    std::cout << tabBig << "choose angle value : <operation>" << std::endl;
    
    std::cout << tab << "2" << std::endl;
    std::cout << tab << "mirrors" << std::endl;
    std::cout << tabBig << "h <- horizontal" << std::endl;
    std::cout << tabBig << "v <- vertical" << std::endl;
    std::cout << tabBig << "all" << std::endl;

    std::cout << tab << "3" << std::endl;
    std::cout << tab << "3x bigger window" << std::endl;
    std::cout << tabBig << "doesnt matter type anything" << std::endl;

    std::cout << tab << "4" << std::endl;
    std::cout << tab << "move sprite" << std::endl;
    std::cout << tabBig << "type vector : <x,y>" << std::endl;

    std::cout << tab << "5" << std::endl;
    std::cout << tab << "rotate origin" << std::endl;
    std::cout << tabBig << "LU,<angle>" << std::endl;
    std::cout << tabBig << "LD" << std::endl;
    std::cout << tabBig << "RU" << std::endl;
    std::cout << tabBig << "RD" << std::endl;

    std::cout << tab << "6" << std::endl;
    std::cout << tab << "arit, cutRange, norm" << std::endl;
    std::cout << tabBig << "arits :" << std::endl;
    std::cout << tabBig << "Aadd" << std::endl;
    std::cout << tabBig << "Asub" << std::endl;
    std::cout << tabBig << "Amul" << std::endl << std::endl;
    std::cout << tabBig << "cutRange :" << std::endl;
    std::cout << tabBig << "Cadd" << std::endl;
    std::cout << tabBig << "Csub" << std::endl;
    std::cout << tabBig << "Cmul" << std::endl << std::endl;
    std::cout << tabBig << "norm :" << std::endl;
    std::cout << tabBig << "Nadd" << std::endl;
    std::cout << tabBig << "Nsub" << std::endl;
    std::cout << tabBig << "Nmul" << std::endl << std::endl;
    std::cout << tabBig << "Mul,Normal same img :" << std::endl;
    std::cout << tabBig << "same" << std::endl << std::endl;
    std::cout << tabBig << "all windows : " << std::endl;
    std::cout << tabBig << "all" << std::endl << std::endl;
}

class MoreWindows {
public:
    MoreWindows() = default;

    MoreWindows(
        const std::string & title, 
        const sf::Sprite & sprite, 
        const sf::Texture & texture, 
        sf::Vector2u size) 
        : savePath(title), sprite(sprite), texture(texture) 
    {
        this->sprite.setTexture(this->texture);
        window.create(sf::VideoMode(size.x, size.y), title, sf::Style::None);
        renderTexture.create(size.x, size.y);
    }

    void screenShot() const 
    {
        renderTexture.getTexture().copyToImage().saveToFile(savePath);
    }

    void run() {

        if (!window.isOpen()) { return; }

        sf::Event event;
        while (window.pollEvent(event)) 
        {
            if (event.type == sf::Event::Closed) { window.close(); }
        }

        renderTexture.clear(sf::Color::Black);
        renderTexture.draw(sprite);
        renderTexture.display();

        window.clear();
        sf::Sprite spr (renderTexture.getTexture());
        window.draw(spr);
        window.display();
    }

    bool isOpen() const {
        return window.isOpen();
    }

private:
    sf::Texture texture;
    const std::string savePath; 
    sf::RenderWindow window;
    sf::RenderTexture renderTexture;
    sf::Sprite sprite;
};

class Application 
{    
    void pushWindow(const std::string & title, const sf::Image & im, sf::Vector2u size) 
    {
        sf::Texture texture;
        texture.loadFromImage(im);
        sf::Sprite sprite;
        sprite.setTexture(texture);
    
        new (&windows[count++]) MoreWindows { title, sprite, texture, size };
    }

    void pushWindow(const std::string & title, const sf::Sprite & sprite, sf::Vector2u size) 
    {
        new (&windows[count++]) MoreWindows { title, sprite, *sprite.getTexture(), size };
    }
public:

    void run(const std::string & task, const std::string & operation)
    {
        const std::string imName = "human";
        sf::Texture txt0, txt1, txt2;
        txt0.loadFromFile("../../zestawnr9/images/all_images/" + imName + "0.png");
        txt1.loadFromFile("../../zestawnr9/images/all_images/" + imName + "1.png");
        txt2.loadFromFile("../../zestawnr9/images/all_images/" + imName + "2.png");
 
        int imgHeight = (txt0.getSize().y);
        int imgWidth  = (txt0.getSize().x);
    
        sf::Image im0, im1, im2;
        im0 = txt0.copyToImage();
        im1 = txt1.copyToImage();
        im2 = txt2.copyToImage();
        
        sf::Texture resultTexture;
        std::pair<int, int> tempSize = std::make_pair(imgWidth,imgHeight);

        // flip 90 180 270
        if(task == "1")
        {
            const int angle = stoi(operation);

            sf::Vector2u size = 
                windowSizeRotation(
                    im0.getSize(), 
                    sf::Vector2f(static_cast<float>(imgHeight)/2, static_cast<float>(imgWidth)/2), 
                    angleRad(angle)
                );

            sf::Image rotatedImg = rotate(im0, getOrigin(im0, Corner::CE), angleRad(angle), size);
            sf::Image rotatedFill = fillGaps(rotatedImg, size);

            pushWindow("../images/1/rotatedImg.jpg", rotatedImg, size);
            pushWindow("../images/1/rotatedImgFill.jpg", rotatedFill, size);
        }
        // horizontal, vertical flip
        else if(task == "2")
        {
            sf::Image mirrorHor = mirrorReflection(im0, Dimension::Horizontal);
            sf::Image mirrorVer = mirrorReflection(im0, Dimension::Vertical);
            
            if(operation == "v")   
            {
                pushWindow("../images/2/Vertical.jpg", mirrorVer, mirrorVer.getSize());
            }
            else if(operation == "h") 
            {
                pushWindow("../images/2/Horizontal.jpg", mirrorHor, mirrorHor.getSize());
            }
            else if(operation == "all")
            {
                pushWindow("../images/2/Vertical.jpg", mirrorVer, mirrorVer.getSize());
                pushWindow("../images/2/Horizontal.jpg", mirrorHor, mirrorHor.getSize());
            }
        }
        // 3x bigger window, sprite center
        else if(task == "3")
        {
            sf::Sprite sprite;
            sprite.setTexture(txt0);
            
            const std::pair<int, int> newSize = 
                resizeWindowAndMoveSprite(3, 3, imgWidth, imgHeight, sprite);

            pushWindow("../images/3/biggerWindow.jpg", 
                sprite, sf::Vector2u(newSize.first,newSize.second));
        }
        // move sprite
        else if(task == "4")
        {
            sf::Sprite sprite;
            sprite.setTexture(txt0);

            const int wndMul = 3;
            const std::pair<int, int> newSize = 
                resizeWindowAndMoveSprite(wndMul, wndMul, imgWidth, imgHeight, sprite);

            const std::string delimiter = ",";
            const std::pair<int,int> position = 
                separateInputMoveValues(delimiter, operation);
            
            moveSprite(sprite, position.first, position.second);
            
            pushWindow("../images/4/movedSprite.jpg", 
                sprite, sf::Vector2u(newSize.first,newSize.second));
        }
        // rotate corner
        else if(task == "5")
        {
            const std::string delimiter = ",";
            std::pair<std::string, int> separatedInput = 
                separateInputOriginAngle(delimiter, operation);
            
            int angle = separatedInput.second;
            std::string corner = separatedInput.first;
            Corner c = convertToEnum(corner);

            sf::Vector2f origin = getOrigin(im0, c);
            sf::Vector2u size = windowSizeRotation(im0.getSize(), origin, angleRad(angle));
            sf::Image rotatedImg = rotate(im0, origin, angleRad(angle), size * 3u);

            pushWindow("../images/5/rotatedImg.jpg", rotatedImg, rotatedImg.getSize());
        }
        // arit, color
        else if(task == "6")
        {
            sf::Image aritAdd    = arit(im0, im1, AritOp::Add);
            sf::Image aritSub    = arit(im0, im1, AritOp::Sub);
            sf::Image aritMul    = arit(im0, im1, AritOp::Mul);
            sf::Image cutRangeAdd      = aritCutRange(im0, im1, AritOp::Add);
            sf::Image cutRangeSub      = aritCutRange(im0, im1, AritOp::Sub);
            sf::Image cutRangeMul      = aritCutRange(im0, im1, AritOp::Mul);
            sf::Image normAdd = norm(aritAdd);
            sf::Image normSub = norm(aritSub);
            sf::Image normMul = norm(aritMul);

            //test purposes same image
            sf::Image normSameImMul = 
                norm(arit(im0,im0, AritOp::Mul));

            //min_values(im0); // test of jpeg is wrong
            //max_values(im0);

            if(operation == "Aadd") 
            {
                pushWindow("../images/6/aritAdd.jpg", 
                aritAdd, aritAdd.getSize());
            }

            else if(operation == "Asub") 
            {
                pushWindow("../images/6/aritSub.jpg", 
                aritSub, aritSub.getSize());
            }

            else if(operation == "Amul") 
            {
                pushWindow("../images/6/aritMul.jpg", 
                aritMul, aritMul.getSize());
            }

            else if(operation == "Cadd") 
            {
                pushWindow("../images/6/cutRangeAdd.jpg", 
                cutRangeAdd, cutRangeAdd.getSize());
            }

            else if(operation == "Csub") 
            {
                pushWindow("../images/6/cutRangeSub.jpg", 
                cutRangeSub, cutRangeSub.getSize());
            }

            else if(operation == "Cmul") 
            {
                pushWindow("../images/6/cutRangeMul.jpg", 
                cutRangeMul, cutRangeMul.getSize());
            }

            else if(operation == "Nadd") 
            {
                pushWindow("../images/6/normAdd.jpg", 
                normAdd, normAdd.getSize());
            }

            else if(operation == "Nsub") 
            {
                pushWindow("../images/6/normSub.jpg", 
                normSub, normSub.getSize());
            }

            else if(operation == "Nmul") 
            {
                pushWindow("../images/6/normMul.jpg", 
                normMul, normMul.getSize());
            }

            else if(operation == "same") 
            {
                pushWindow("../images/6/normSameImMul.jpg", 
                normSameImMul, normSameImMul.getSize());
            }

            else if(operation == "all")
            {
                pushWindow("../images/6/aritAdd.jpg", 
                    aritAdd, aritAdd.getSize());
                pushWindow("../images/6/cutRangeAdd.jpg", 
                    cutRangeAdd, cutRangeAdd.getSize());
                pushWindow("../images/6/normAdd.jpg", 
                    normAdd, normAdd.getSize());
                pushWindow("../images/6/aritSub.jpg", 
                    aritSub, aritSub.getSize());
                pushWindow("../images/6/cutRangeSub.jpg", 
                    cutRangeSub, cutRangeSub.getSize());
                pushWindow("../images/6/normSub.jpg", 
                    normSub, normSub.getSize());
                pushWindow("../images/6/aritMul.jpg", 
                    aritMul, aritMul.getSize());
                pushWindow("../images/6/cutRangeMul.jpg", 
                    cutRangeMul, cutRangeMul.getSize());
                pushWindow("../images/6/normMul.jpg", 
                    normMul, normMul.getSize());
                pushWindow("../images/6/normSameImMul.jpg", 
                    normSameImMul, normSameImMul.getSize());
            }
        }
        else if(task == "con")
        {
            sf::Image c0 = conToNeg(im0);
            sf::Image c1 = conToNeg(im1);
            sf::Image c2 = conToNeg(im2);
            //        txt2.loadFromFile("../../zestawnr9/images/all_images/cat2.png");

            pushWindow("../../zestawnr9/images/all_images/" + imName + "Con0.png", c0, im0.getSize());
            pushWindow("../../zestawnr9/images/all_images/" + imName + "Con1.png", c1, im1.getSize());
            pushWindow("../../zestawnr9/images/all_images/" + imName + "Con2.png", c2, im2.getSize());
        }
        else
        {
            pushWindow("../images/default.jpg", im0, im0.getSize());
            std::cout << "default image shown" << std::endl;
        }

        while (true) 
        {
            std::size_t open = 0u;
            
            for (std::size_t i = 0u; i < count; i++) 
            {
                if (windows[i].isOpen()) { open++; }
            }

            if (open == 0) {
                break;
            }

            for (std::size_t i = 0u; i < count; i++) {
                windows[i].run();
            }
        }

        for (std::size_t i = 0u; i < count; i++) 
        {
            windows[i].screenShot();
        }
    }

private:
    std::array<MoreWindows, 10> windows = {};
    std::size_t count = 0;
};

int main (int argc, char* argv[])
{
    menu();

    if (argc != 3)
    {
        std::cerr << "Usage: ./zestawnr2 <task> <operation/all>" << std::endl;
        return 1;
    }

    std::string task(argv[1]);
    std::string operation(argv[2]);
    Application app;
    app.run(task, operation);

    return 0;
}

// TODO :
/*
    rotating origin, creates slightly bigger window
*/
