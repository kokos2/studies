#include <functions.hpp>
#include <array>
#include <iostream>

void menu()
{
    const std::string tab = "        ";
    const std::string tabBig = "                     ";
}

class MoreWindows {
public:
    MoreWindows() = default;

    MoreWindows(
        const std::string & title, 
        const sf::Sprite & sprite, 
        const sf::Texture & texture, 
        sf::Vector2u size) 
        : savePath(title), sprite(sprite), texture(texture) 
    {
        this->sprite.setTexture(this->texture);
        window.create(sf::VideoMode(size.x, size.y), title, sf::Style::None);
        renderTexture.create(size.x, size.y);
    }

    void screenShot() const 
    {
        renderTexture.getTexture().copyToImage().saveToFile(savePath);
    }

    void run() {

        if (!window.isOpen()) { return; }

        sf::Event event;
        while (window.pollEvent(event)) 
        {
            if (event.type == sf::Event::Closed) { window.close(); }
        }

        renderTexture.clear(sf::Color::Black);
        renderTexture.draw(sprite);
        renderTexture.display();

        window.clear();
        sf::Sprite spr (renderTexture.getTexture());
        window.draw(spr);
        window.display();
    }

    bool isOpen() const {
        return window.isOpen();
    }

private:
    sf::Texture texture;
    const std::string savePath; 
    sf::RenderWindow window;
    sf::RenderTexture renderTexture;
    sf::Sprite sprite;
};

class Application 
{    
    void pushWindow(const std::string & title, const sf::Image & im, sf::Vector2u size) 
    {
        sf::Texture texture;
        texture.loadFromImage(im);
        sf::Sprite sprite;
        sprite.setTexture(texture);
    
        new (&windows[count++]) MoreWindows { title, sprite, texture, size };
    }

    void pushWindow(const std::string & title, const sf::Sprite & sprite, sf::Vector2u size) 
    {
        new (&windows[count++]) MoreWindows { title, sprite, *sprite.getTexture(), size };
    }
    
public:

    void run(const std::string & task, const std::string & operation)
    {
        sf::Image im0, im1;
        im0.loadFromFile("../../images/arch.jpg");
        im1.loadFromFile("../../images/papuga.jpg");
        
        int imgHeight = (im0.getSize().y);
        int imgWidth  = (im0.getSize().x);
        
        sf::Texture resultTexture;
        std::pair<int, int> tempSize = std::make_pair(imgWidth,imgHeight);

        /*
            upper filters
        */

        const float filter3[3][3] = { 
            { 1, 1, 1 },
            { 1, 1, 1 },
            { 1, 1, 1 },
        };

        const float filter5[5][5] = { 
            { 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1 }
        };

        const float filter7[7][7] = { 
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 }
        };
        
        sf::Image PS5  = noisePeperSalt(im1, 0.05f);
        sf::Image PS15 = noisePeperSalt(im1, 0.15f);
        sf::Image PS40 = noisePeperSalt(im1, 0.40f);
            
        sf::Image g5  = gaussianNoise(im1, 0.05f);
        sf::Image g15 = gaussianNoise(im1, 0.15f);
        sf::Image g40 = gaussianNoise(im1, 0.40f);

        // pepper salt 5/15/40 && gaussian
        if(task == "1") 
        {
            // moved upper scope
            pushWindow("../images/1/PS5.jpg", PS5, PS5.getSize());
            pushWindow("../images/1/PS15.jpg", PS15, PS15.getSize());
            pushWindow("../images/1/PS40.jpg", PS40, PS40.getSize());

            pushWindow("../images/1/g5.jpg", g5, g5.getSize());
            pushWindow("../images/1/g15.jpg", g15, g15.getSize());
            pushWindow("../images/1/g40.jpg", g40, g40.getSize());
        }
        // m 3
        else if(task == "2")
        {   
            sf::Image m3ps5  = applyMedian(PS5 , filter3);
            sf::Image m3ps15 = applyMedian(PS15, filter3);
            sf::Image m3ps40 = applyMedian(PS40, filter3);
            pushWindow("../images/2/m3ps5.jpg",  m3ps5,  m3ps5.getSize());
            pushWindow("../images/2/m3ps15.jpg", m3ps15, m3ps15.getSize());
            pushWindow("../images/2/m3ps40.jpg", m3ps40, m3ps40.getSize());
            
            sf::Image m3g5  = applyMedian(g5 , filter3);
            sf::Image m3g15 = applyMedian(g15, filter3);
            sf::Image m3g40 = applyMedian(g40, filter3);
            pushWindow("../images/2/m3g5.jpg", m3g5, m3g5.getSize());
            pushWindow("../images/2/m3g15.jpg", m3g15, m3g15.getSize());
            pushWindow("../images/2/m3g40.jpg", m3g40, m3g40.getSize());
        }
        // m 5
        else if(task == "3")
        {
            sf::Image m5ps5  = applyMedian(PS5 , filter5);
            sf::Image m5ps15 = applyMedian(PS15, filter5);
            sf::Image m5ps40 = applyMedian(PS40, filter5);
            pushWindow("../images/3/m5ps5.jpg",  m5ps5,  m5ps5.getSize());
            pushWindow("../images/3/m5ps15.jpg", m5ps15, m5ps15.getSize());
            pushWindow("../images/3/m5ps40.jpg", m5ps40, m5ps40.getSize());
            
            sf::Image m5g5  = applyMedian(g5 , filter5);
            sf::Image m5g15 = applyMedian(g15, filter5);
            sf::Image m5g40 = applyMedian(g40, filter5);
            pushWindow("../images/3/m5g5.jpg", m5g5, m5g5.getSize());
            pushWindow("../images/3/m5g15.jpg", m5g15, m5g15.getSize());
            pushWindow("../images/3/m5g40.jpg", m5g40, m5g40.getSize());
        }
        // m 7
        else if(task == "4")
        {
            sf::Image m7ps5  = applyMedian(PS5 , filter7);
            sf::Image m7ps15 = applyMedian(PS15, filter7);
            sf::Image m7ps40 = applyMedian(PS40, filter7);
            pushWindow("../images/4/m7ps5.jpg",  m7ps5,  m7ps5.getSize());
            pushWindow("../images/4/m7ps15.jpg", m7ps15, m7ps15.getSize());
            pushWindow("../images/4/m7ps40.jpg", m7ps40, m7ps40.getSize());
            
            sf::Image m7g5  = applyMedian(g5 , filter7);
            sf::Image m7g15 = applyMedian(g15, filter7);
            sf::Image m7g40 = applyMedian(g40, filter7);
            pushWindow("../images/4/m7g5.jpg", m7g5, m7g5.getSize());
            pushWindow("../images/4/m7g15.jpg", m7g15, m7g15.getSize());
            pushWindow("../images/4/m7g40.jpg", m7g40, m7g40.getSize());
        }
        // weight median
        else if(task == "5")
        {   
            // centralFilterSizeWeight  
            const float cfs3w00[3][3] = { 
                { 1, 1, 1 },
                { 1, 1, 1 },
                { 1, 1, 1 },
            };    
            const float cfs3w50[3][3] = { 
                { 1, 1, 1 },
                { 1, 4, 1 },
                { 1, 1, 1 },
            };
            const float cfs3w90[3][3] = { 
                { 1, 1, 1 },
                { 1, 8, 1 },
                { 1, 1, 1 },
            };
            const float cfs5w00[5][5] = { 
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 }
            };
            const float cfs5w50[5][5] = { 
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 1, 12, 1, 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 }
            };
            const float cfs5w90[5][5] = { 
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 1, 22, 1, 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1 }
            };
            const float cfs7w00[7][7] = { 
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 }
            };
            const float cfs7w50[7][7] = { 
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 25, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 }
            };
            const float cfs7w90[7][7] = { 
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 44, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 },
                { 1, 1, 1, 1, 1, 1, 1 }
            };
            // 0 -> 50 - > 90
            sf::Image i0, i1, i2;
            i0.loadFromFile("../../images/maks2.jpg");
            i1.loadFromFile("../../images/kamil.jpg");
            i2.loadFromFile("../../images/arch.jpg");
            
            sf::Image i0g = grayScale(i0);
            sf::Image i1g = grayScale(i1);
            sf::Image i2g = grayScale(i2);

            pushWindow("../images/5/i0g.jpg", i0g, i0g.getSize());
            pushWindow("../images/5/i1g.jpg", i1g, i1g.getSize());
            pushWindow("../images/5/i2g.jpg", i2g, i2g.getSize());
            
            // first im - > more incestigation
            // m3
            sf::Image m3w00 = applyMedian(grayScale(i0), cfs3w00);
            sf::Image m3w50 = applyMedian(grayScale(i0), cfs3w50);
            sf::Image m3w90 = applyMedian(grayScale(i0), cfs3w90);
            pushWindow("../images/5/m3w00.jpg", m3w00, m3w00.getSize());
            pushWindow("../images/5/m3w50.jpg", m3w50, m3w50.getSize());
            pushWindow("../images/5/m3w90.jpg", m3w90, m3w90.getSize());

            // m5
            sf::Image m5w00 = applyMedian(grayScale(i0), cfs5w00);
            sf::Image m5w50 = applyMedian(grayScale(i0), cfs5w50);
            sf::Image m5w90 = applyMedian(grayScale(i0), cfs5w90);
            pushWindow("../images/5/m5w00.jpg", m5w00, m5w00.getSize());
            pushWindow("../images/5/m5w50.jpg", m5w50, m5w50.getSize());
            pushWindow("../images/5/m5w90.jpg", m5w90, m5w90.getSize());

            // m7
            sf::Image m7w00 = applyMedian(grayScale(i0), cfs7w00);
            sf::Image m7w50 = applyMedian(grayScale(i0), cfs7w50);
            sf::Image m7w90 = applyMedian(grayScale(i0), cfs7w90);
            pushWindow("../images/5/m7w00.jpg", m7w00, m7w00.getSize());
            pushWindow("../images/5/m7w50.jpg", m7w50, m7w50.getSize());
            pushWindow("../images/5/m7w90.jpg", m7w90, m7w90.getSize());

            // sec im - > test purposes, fill requirements
            sf::Image kamilM5w50 = applyMedian(grayScale(i1), cfs5w50);
            sf::Image kamilM5w90 = applyMedian(grayScale(i1), cfs5w90);
            pushWindow("../images/5/kamilM5w50.jpg", kamilM5w50, kamilM5w50.getSize());
            pushWindow("../images/5/kamilM5w90.jpg", kamilM5w90, kamilM5w90.getSize());
            
            // third im - > same as previous situation
            sf::Image archM7w00 = applyMedian(grayScale(i2), cfs7w00);
            sf::Image archM7w50 = applyMedian(grayScale(i2), cfs7w50);
            pushWindow("../images/5/archM7w00.jpg", archM7w00, archM7w00.getSize());
            pushWindow("../images/5/archM7w50.jpg", archM7w50, archM7w50.getSize());
            /*
                weighted median
                int weights
                test on no noise
                central weights - diffrent weights - 3 examples
                
                my thoughs
                1.
                if central point is bigger than rest of weights
                img is going to stay the same

                2.
                smoothing edges?

                3.
                blurring

                4. 
                lower center -> more blurr

                5.
                very long executing program

                6.
                bigger mask -> strongly lower simmiliarity to original img
            */
        }
        // some experimetns
        else if(task == "6")
        {

        }
        else
        {   
            pushWindow("../images/default.jpg", im0, im0.getSize());
            std::cout << "default image shown" << std::endl;
        }

        while (true) 
        {
            std::size_t open = 0u;
            
            for (std::size_t i = 0u; i < count; i++) 
            {
                if (windows[i].isOpen()) { open++; }
            }

            if (open == 0) {
                break;
            }

            for (std::size_t i = 0u; i < count; i++) {
                windows[i].run();
            }
        }

        for (std::size_t i = 0u; i < count; i++) 
        {
            windows[i].screenShot();
        }
    }

private:
    std::array<MoreWindows, 20> windows = {};
    std::size_t count = 0;
};

int main (int argc, char* argv[])
{
    menu();

    if (argc != 3)
    {
        std::cerr << "Usage: ./zestawnr <task> <operation/all>" << std::endl;
        return 1;
    }

    std::string task(argv[1]);
    std::string operation(argv[2]);
    Application app;
    app.run(task, operation);

    return 0;
}
