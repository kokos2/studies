#include <functions.hpp>
#include <array>
#include <iostream>

void menu()
{
    const std::string tab = "        ";
    const std::string tabBig = "                     ";
}

class MoreWindows {
public:
    MoreWindows() = default;

    MoreWindows(
        const std::string & title, 
        const sf::Sprite & sprite, 
        const sf::Texture & texture, 
        sf::Vector2u size) 
        : savePath(title), sprite(sprite), texture(texture) 
    {
        this->sprite.setTexture(this->texture);
        window.create(sf::VideoMode(size.x, size.y), title, sf::Style::None);
        renderTexture.create(size.x, size.y);
    }

    void screenShot() const 
    {
        renderTexture.getTexture().copyToImage().saveToFile(savePath);
    }

    void run() {

        if (!window.isOpen()) { return; }

        sf::Event event;
        while (window.pollEvent(event)) 
        {
            if (event.type == sf::Event::Closed) { window.close(); }
        }

        renderTexture.clear(sf::Color::Black);
        renderTexture.draw(sprite);
        renderTexture.display();

        window.clear();
        sf::Sprite spr (renderTexture.getTexture());
        window.draw(spr);
        window.display();
    }

    bool isOpen() const {
        return window.isOpen();
    }

private:
    sf::Texture texture;
    const std::string savePath; 
    sf::RenderWindow window;
    sf::RenderTexture renderTexture;
    sf::Sprite sprite;
};

class Application 
{    
    void pushWindow(const std::string & title, const sf::Image & im, sf::Vector2u size) 
    {
        sf::Texture texture;
        texture.loadFromImage(im);
        sf::Sprite sprite;
        sprite.setTexture(texture);
    
        new (&windows[count++]) MoreWindows { title, sprite, texture, size };
    }

    void pushWindow(const std::string & title, const sf::Sprite & sprite, sf::Vector2u size) 
    {
        new (&windows[count++]) MoreWindows { title, sprite, *sprite.getTexture(), size };
    }
    
public:

    void run(const std::string & task, const std::string & operation)
    {
        sf::Image im0, im1;
        im0.loadFromFile("../../images/arch.jpg");
        im1.loadFromFile("../../images/prezka.png");
        
        int imgHeight = (im0.getSize().y);
        int imgWidth  = (im0.getSize().x);
        
        sf::Texture resultTexture;
        std::pair<int, int> tempSize = std::make_pair(imgWidth,imgHeight);

        if(task == "1")
        {
            sf::Vector2f scale = {1.28f, 0.86f};
            sf::Vector2u imSize = windowSizeScale(im0, scale);
            
            sf::Image scaled = scaleIm(im0, imSize, scale);
            sf::Image scaledFilled = fillGaps(im0, imSize);

            pushWindow("../images/scaled.jpg", scaled, scaled.getSize());
            pushWindow("../images/scaledFilled.jpg", scaledFilled, scaledFilled.getSize());
        }
        else if(task == "2")
        {
            std::cout << "did in lab2" << std::endl;
        }
        else if(task == "3")
        {
            std::cout << "did in lab2" << std::endl;
        }
        else if(task == "4")
        {
            float angle = angleRad(stof(operation, nullptr));
            Basis basis = Basis::Left;

            sf::Vector2u size = windowSizeSkew(im0.getSize(), basis, angle);
            std::cout << "size x : " << size.x << " y : " << size.y << std::endl;

            sf::Image skewed = skew(im0, size, angle, basis);

            pushWindow("../images/skew.jpg", skewed, skewed.getSize());
        }
        else if(task == "5")
        {
            // did in 4
        }
        else if(task == "6")
        {
            im0.loadFromFile("../images/lab3_biurko.jpg");

            const auto test = findGreenVertices(im0, 50, sf::Color(82,254,84));
            
            for (const auto & it : test) 
            {
                std::cout << "x: " << it.x << " y: " << it.y << std::endl;
            }
            sf::Image greenScreen = fillGreenScreen(im0, im1);

            float filter[5][5] = { { 0, 0, -1, 0, 0},{ 0, 0, -1, 0, 0},{ -1, -1, 9, -1, -1},{ 0, 0, -1, 0, 0},{ 0, 0, -1, 0, 0}};
            //float filter[5][5] = { { 1, 1, 1, 1, 1},{ 1, 1, 1, 1, 1},{ 1, 1, 2, 1, 1},{ 1, 1, 1, 1, 1},{ 1, 1, 1, 1, 1}};
            //float filter[3][3] = {{-1,0,1},{-1,1,1},{-1,0,1}}; // plaskorzezby
            //float filter[3][3] = {{1,1,1},{1,2,1},{1,1,1}}; // dolno
            //float filter[3][3] = {{1,2,1},{2,4,2},{1,2,1}}; // dolno
            //float filter[3][3] = {{0,1,0},{1,-4,1},{0,1,0}};  
            //float filter[3][3] = {{-1,1,1},{-1,-2,1},{-1,1,1}};  
            sf::Image maskTest = applyFilter(im1, filter);
            //pushWindow("../images/fill.jpg", greenScreen, greenScreen.getSize());
            pushWindow("../images/fill.jpg", maskTest, maskTest.getSize());
        }
        else
        {
            
            pushWindow("../images/default.jpg", im0, im0.getSize());
            std::cout << "default image shown" << std::endl;
        }

        while (true) 
        {
            std::size_t open = 0u;
            
            for (std::size_t i = 0u; i < count; i++) 
            {
                if (windows[i].isOpen()) { open++; }
            }

            if (open == 0) {
                break;
            }

            for (std::size_t i = 0u; i < count; i++) {
                windows[i].run();
            }
        }

        for (std::size_t i = 0u; i < count; i++) 
        {
            windows[i].screenShot();
        }
    }

private:
    std::array<MoreWindows, 10> windows = {};
    std::size_t count = 0;
};

int main (int argc, char* argv[])
{
    menu();

    if (argc != 3)
    {
        std::cerr << "Usage: ./zestawnr <task> <operation/all>" << std::endl;
        return 1;
    }

    std::string task(argv[1]);
    std::string operation(argv[2]);
    Application app;
    app.run(task, operation);

    return 0;
}
