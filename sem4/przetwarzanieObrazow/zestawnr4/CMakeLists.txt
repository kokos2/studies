cmake_minimum_required(VERSION 3.25)
project(zestawnr4)

add_executable(zestawnr4 src/main.cpp)

target_compile_features(zestawnr4 PRIVATE cxx_std_17)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../functions ${CMAKE_CURRENT_BINARY_DIR}/functions)

target_link_libraries(zestawnr4
    PRIVATE
        functions
)
