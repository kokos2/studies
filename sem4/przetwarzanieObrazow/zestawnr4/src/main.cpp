#include <functions.hpp>
#include <array>
#include <iostream>

void menu()
{
    const std::string tab = "        ";
    const std::string tabBig = "                     ";
}

class MoreWindows {
public:
    MoreWindows() = default;

    MoreWindows(
        const std::string & title, 
        const sf::Sprite & sprite, 
        const sf::Texture & texture, 
        sf::Vector2u size) 
        : savePath(title), sprite(sprite), texture(texture) 
    {
        this->sprite.setTexture(this->texture);
        window.create(sf::VideoMode(size.x, size.y), title, sf::Style::None);
        renderTexture.create(size.x, size.y);
    }

    void screenShot() const 
    {
        renderTexture.getTexture().copyToImage().saveToFile(savePath);
    }

    void run() {

        if (!window.isOpen()) { return; }

        sf::Event event;
        while (window.pollEvent(event)) 
        {
            if (event.type == sf::Event::Closed) { window.close(); }
        }

        renderTexture.clear(sf::Color::Black);
        renderTexture.draw(sprite);
        renderTexture.display();

        window.clear();
        sf::Sprite spr (renderTexture.getTexture());
        window.draw(spr);
        window.display();
    }

    bool isOpen() const {
        return window.isOpen();
    }

private:
    sf::Texture texture;
    const std::string savePath; 
    sf::RenderWindow window;
    sf::RenderTexture renderTexture;
    sf::Sprite sprite;
};

class Application 
{    
    void pushWindow(const std::string & title, const sf::Image & im, sf::Vector2u size) 
    {
        sf::Texture texture;
        texture.loadFromImage(im);
        sf::Sprite sprite;
        sprite.setTexture(texture);
    
        new (&windows[count++]) MoreWindows { title, sprite, texture, size };
    }

    void pushWindow(const std::string & title, const sf::Sprite & sprite, sf::Vector2u size) 
    {
        new (&windows[count++]) MoreWindows { title, sprite, *sprite.getTexture(), size };
    }
    
public:

    void run(const std::string & task, const std::string & operation)
    {
        sf::Image im0, im1;
        im0.loadFromFile("../../images/arch.jpg");
        im1.loadFromFile("../../images/maks3.jpg");
        
        int imgHeight = (im0.getSize().y);
        int imgWidth  = (im0.getSize().x);
        
        sf::Texture resultTexture;
        std::pair<int, int> tempSize = std::make_pair(imgWidth,imgHeight);
        
        // low pass
        const float filter3[3][3] = { 
            { 1, 1, 1 },
            { 1, 2, 1 },
            { 1, 1, 1 },
        };

        const float filter5[5][5] = { 
            { 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1 },
            { 1, 1, 2, 1, 1 },
            { 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1 }
        };

        const float filter7[7][7] = { 
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 2, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1, 1, 1 }
        };


        // works also for rgb img
        if(task == "1") // mask 3/5/7   
        {           
            // rewrite
            sf::Image lowPass3default = applyFilter(im1, filter3);
            sf::Image lowPass5default = applyFilter(im1, filter5);
            sf::Image lowPass7default = applyFilter(im1, filter7);
            
            pushWindow("../images/1/lowPass3default.jpg", lowPass3default, lowPass3default.getSize());
            pushWindow("../images/1/lowPass5default.jpg", lowPass5default, lowPass5default.getSize());
            pushWindow("../images/1/lowPass7default.jpg", lowPass7default, lowPass7default.getSize());
            
            // bigger size 
            sf::Image bigIm3 = additionalBorders(im1, std::size(filter3[0]));
            sf::Image bigIm5 = additionalBorders(im1, std::size(filter5[0]));
            sf::Image bigIm7 = additionalBorders(im1, std::size(filter7[0]));

            sf::Image lowPass3big = applyFilter(bigIm3, filter3);
            sf::Image lowPass5big = applyFilter(bigIm5, filter5);
            sf::Image lowPass7big = applyFilter(bigIm7, filter7);
            
            pushWindow("../images/1/lowPass3big.jpg", lowPass3big, lowPass3big.getSize());
            pushWindow("../images/1/lowPass5big.jpg", lowPass5big, lowPass5big.getSize());
            pushWindow("../images/1/lowPass7big.jpg", lowPass7big, lowPass7big.getSize());
        }
        // peper salt 2/5/15 % mask 3/5/7
        else if(task == "2")
        {
            // default
            sf::Image PS2  = noisePeperSalt(im1, 0.02f);
            sf::Image PS5  = noisePeperSalt(im1, 0.05f);
            sf::Image PS15 = noisePeperSalt(im1, 0.15f);
            pushWindow("../images/2/PS2.jpg" , PS2 , PS2.getSize());
            pushWindow("../images/2/PS5.jpg" , PS5 , PS5.getSize());
            pushWindow("../images/2/PS15.jpg" , PS15 , PS15.getSize());

            // bigger size 
            sf::Image bigIm3 = additionalBorders(PS2 , std::size(filter3[0]));
            sf::Image bigIm5 = additionalBorders(PS5 , std::size(filter5[0]));
            sf::Image bigIm7 = additionalBorders(PS15, std::size(filter7[0]));

            // default sec
            // 3x3
            sf::Image PS2m3 = applyFilter(PS2, filter3);
            sf::Image PS5m3 = applyFilter(PS5, filter3);
            sf::Image PS15m3 = applyFilter(PS15, filter3);
            // 5x5
            sf::Image PS2m5 = applyFilter(PS2, filter5);
            sf::Image PS5m5 = applyFilter(PS5, filter5);
            sf::Image PS15m5 = applyFilter(PS15, filter5);
            // 7x7
            sf::Image PS2m7 = applyFilter(PS2, filter7);
            sf::Image PS5m7 = applyFilter(PS5, filter7);
            sf::Image PS15m7 = applyFilter(PS15, filter7);
            pushWindow("../images/2/PS2m3.jpg" , PS2m3 , PS2m3.getSize());
            pushWindow("../images/2/PS5m3.jpg" , PS5m3 , PS5m3.getSize());
            pushWindow("../images/2/PS15m3.jpg" , PS15m3 , PS15m3.getSize());
            
            pushWindow("../images/2/PS2m5.jpg" , PS2m5 , PS2m5.getSize());
            pushWindow("../images/2/PS5m5.jpg" , PS5m5 , PS5m5.getSize());
            pushWindow("../images/2/PS15m5.jpg" , PS15m5 , PS15m5.getSize());
            
            pushWindow("../images/2/PS2m7.jpg" , PS2m7 , PS2m7.getSize());
            pushWindow("../images/2/PS5m7.jpg" , PS5m7 , PS5m7.getSize());
            pushWindow("../images/2/PS15m7.jpg" , PS15m7 , PS15m7.getSize());

            // big section
            // 3x3
            sf::Image PSBig2m3 = applyFilter(bigIm3, filter3);
            sf::Image PSBig5m3 = applyFilter(bigIm5, filter3);
            sf::Image PSBig15m3 = applyFilter(bigIm7, filter3);
            // 5x5
            sf::Image PSBig2m5 = applyFilter(bigIm3, filter5);
            sf::Image PSBig5m5 = applyFilter(bigIm5, filter5);
            sf::Image PSBig15m5 = applyFilter(bigIm7, filter5);
            // 7x7
            sf::Image PSBig2m7 = applyFilter(bigIm3, filter7);
            sf::Image PSBig5m7 = applyFilter(bigIm5, filter7);
            sf::Image PSBig15m7 = applyFilter(bigIm7, filter7);
            pushWindow("../images/2/PSBig2m3.jpg" , PSBig2m3 , PSBig2m3.getSize());
            pushWindow("../images/2/PSBig5m3.jpg" , PSBig5m3 , PSBig5m3.getSize());
            pushWindow("../images/2/PSBig15m3.jpg" , PSBig15m3 , PSBig15m3.getSize());
            
            pushWindow("../images/2/PSBig2m5.jpg" , PSBig2m5 , PSBig2m5.getSize());
            pushWindow("../images/2/PSBig5m5.jpg" , PSBig5m5 , PSBig5m5.getSize());
            pushWindow("../images/2/PSBig15m5.jpg" , PSBig15m5 , PSBig15m5.getSize());
            
            pushWindow("../images/2/PSBig2m7.jpg" , PSBig2m7 , PSBig2m7.getSize());
            pushWindow("../images/2/PSBig5m7.jpg" , PSBig5m7 , PSBig5m7.getSize());
            pushWindow("../images/2/PSBig15m7.jpg" , PSBig15m7 , PSBig15m7.getSize());

            // push ims
        }
        // gauss noise 2/5/15% 3/5/7 masks
        else if(task == "3")
        {
            // default
            sf::Image g2  = gaussianNoise(im1, 0.02f);
            sf::Image g5  = gaussianNoise(im1, 0.05f);
            sf::Image g15 = gaussianNoise(im1, 0.15f);

            pushWindow("../images/3/g2.jpg" , g2 , g2.getSize());
            pushWindow("../images/3/g5.jpg" , g5 , g5.getSize());
            pushWindow("../images/3/g15.jpg", g15, g15.getSize());

            // bigger size 
            sf::Image bigIm3 = additionalBorders(g2 , std::size(filter3[0]));
            sf::Image bigIm5 = additionalBorders(g5 , std::size(filter5[0]));
            sf::Image bigIm7 = additionalBorders(g15, std::size(filter7[0]));

            // default sec
            // 3x3
            sf::Image g2m3 = applyFilter(g2, filter3);
            sf::Image g5m3 = applyFilter(g5, filter3);
            sf::Image g15m3 = applyFilter(g15, filter3);
            // 5x5
            sf::Image g2m5 = applyFilter(g2, filter5);
            sf::Image g5m5 = applyFilter(g5, filter5);
            sf::Image g15m5 = applyFilter(g15, filter5);
            // 7x7
            sf::Image g2m7 = applyFilter(g2, filter7);
            sf::Image g5m7 = applyFilter(g5, filter7);
            sf::Image g15m7 = applyFilter(g15, filter7);
            pushWindow("../images/3/g2m3.jpg" , g2m3 , g2m3.getSize());
            pushWindow("../images/3/g5m3.jpg" , g5m3 , g5m3.getSize());
            pushWindow("../images/3/g15m3.jpg" , g15m3 , g15m3.getSize());
            
            pushWindow("../images/3/g2m5.jpg" , g2m5 , g2m5.getSize());
            pushWindow("../images/3/g5m5.jpg" , g5m5 , g5m5.getSize());
            pushWindow("../images/3/g15m5.jpg" , g15m5 , g15m5.getSize());
            
            pushWindow("../images/3/g2m7.jpg" , g2m7 , g2m7.getSize());
            pushWindow("../images/3/g5m7.jpg" , g5m7 , g5m7.getSize());
            pushWindow("../images/3/g15m7.jpg" , g15m7 , g15m7.getSize());

            // big section
            // 3x3
            sf::Image gBig2m3 = applyFilter(bigIm3, filter3);
            sf::Image gBig5m3 = applyFilter(bigIm5, filter3);
            sf::Image gBig15m3 = applyFilter(bigIm7, filter3);
            // 5x5
            sf::Image gBig2m5 = applyFilter(bigIm3, filter5);
            sf::Image gBig5m5 = applyFilter(bigIm5, filter5);
            sf::Image gBig15m5 = applyFilter(bigIm7, filter5);
            // 7x7
            sf::Image gBig2m7 = applyFilter(bigIm3, filter7);
            sf::Image gBig5m7 = applyFilter(bigIm5, filter7);
            sf::Image gBig15m7 = applyFilter(bigIm7, filter7);
            pushWindow("../images/3/gBig2m3.jpg" , gBig2m3 , gBig2m3.getSize());
            pushWindow("../images/3/gBig5m3.jpg" , gBig5m3 , gBig5m3.getSize());
            pushWindow("../images/3/gBig15m3.jpg" , gBig15m3 , gBig15m3.getSize());
            
            pushWindow("../images/3/gBig2m5.jpg" , gBig2m5 , gBig2m5.getSize());
            pushWindow("../images/3/gBig5m5.jpg" , gBig5m5 , gBig5m5.getSize());
            pushWindow("../images/3/gBig15m5.jpg" , gBig15m5 , gBig15m5.getSize());
            
            pushWindow("../images/3/gBig2m7.jpg" , gBig2m7 , gBig2m7.getSize());
            pushWindow("../images/3/gBig5m7.jpg" , gBig5m7 , gBig5m7.getSize());
            pushWindow("../images/3/gBig15m7.jpg" , gBig15m7 , gBig15m7.getSize());

        }
        // gaussian filter img->gray / peper salt / gauss 2/5/15%
        else if(task == "4")
        {
            const float gFilter[3][3] = { 
                { 1, 2, 1 },
                { 2, 4, 2 },
                { 1, 2, 1 },
            };

            // point a
                sf::Image bottomG = applyFilter(im1, gFilter);
                pushWindow("../images/4/small/bottomG.jpg", bottomG, bottomG.getSize());

                sf::Image PS2  = noisePeperSalt(im1, 0.02f);
                sf::Image PS5  = noisePeperSalt(im1, 0.05f);
                sf::Image PS15 = noisePeperSalt(im1, 0.15f);
                
                sf::Image gMaskPS2  = applyFilter(PS2, gFilter);
                sf::Image gMaskPS5  = applyFilter(PS5, gFilter);
                sf::Image gMaskPS15 = applyFilter(PS15, gFilter);
                
                pushWindow("../images/4/small/gMaskPS2.jpg", gMaskPS2, gMaskPS2.getSize());
                pushWindow("../images/4/small/gMaskPS5.jpg", gMaskPS5, gMaskPS5.getSize());
                pushWindow("../images/4/small/gMaskPS15.jpg", gMaskPS15, gMaskPS15.getSize());

                sf::Image g2  = gaussianNoise(im1, 0.02f);
                sf::Image g5  = gaussianNoise(im1, 0.05f);
                sf::Image g15 = gaussianNoise(im1, 0.15f);

                sf::Image gMaskG2  = applyFilter(g2, gFilter);
                sf::Image gMaskG5  = applyFilter(g5, gFilter);
                sf::Image gMaskG15 = applyFilter(g15, gFilter);
                
                pushWindow("../images/4/small/gMaskG2.jpg", gMaskG2, gMaskG2.getSize());
                pushWindow("../images/4/small/gMaskG5.jpg", gMaskG5, gMaskG5.getSize());
                pushWindow("../images/4/small/gMaskG15.jpg", gMaskG15, gMaskG15.getSize());

            // point b
                sf::Image bigG = additionalBorders(im1 , std::size(gFilter[0]));
                sf::Image topG = applyFilter(bigG, gFilter);
                pushWindow("../images/4/big/topG.jpg", topG, topG.getSize());

                sf::Image PSbig2  = noisePeperSalt(bigG, 0.02f);
                sf::Image PSbig5  = noisePeperSalt(bigG, 0.05f);
                sf::Image PSbig15 = noisePeperSalt(bigG, 0.15f);
                
                sf::Image gMaskPSbig2  = applyFilter(PSbig2, gFilter);
                sf::Image gMaskPSbig5  = applyFilter(PSbig5, gFilter);
                sf::Image gMaskPSbig15 = applyFilter(PSbig15, gFilter);
                
                pushWindow("../images/4/big/gMaskPSbig2.jpg", gMaskPSbig2, gMaskPSbig2.getSize());
                pushWindow("../images/4/big/gMaskPSbig5.jpg", gMaskPSbig5, gMaskPSbig5.getSize());
                pushWindow("../images/4/big/gMaskPSbig15.jpg", gMaskPSbig15, gMaskPSbig15.getSize());

                sf::Image bigG2  = gaussianNoise(bigG, 0.02f);
                sf::Image bigG5  = gaussianNoise(bigG, 0.05f);
                sf::Image bigG15 = gaussianNoise(bigG, 0.15f);

                sf::Image bigGMaskG2  = applyFilter(bigG2, gFilter);
                sf::Image bigGMaskG5  = applyFilter(bigG5, gFilter);
                sf::Image bigGMaskG15 = applyFilter(bigG15, gFilter);
                
                pushWindow("../images/4/big/gMaskG2.jpg", bigGMaskG2, bigGMaskG2.getSize());
                pushWindow("../images/4/big/gMaskG5.jpg", bigGMaskG5, bigGMaskG5.getSize());
                pushWindow("../images/4/big/gMaskG15.jpg", bigGMaskG15, bigGMaskG15.getSize());
        }
        // my experiments
        else if (task == "5")
        {
            sf::Image imD = im0;
            im1 = grayScale(im0);
            const float bigFilter[251][251] = {};
            
            sf::Image bigLinuxColr = additionalBorders(imD, std::size(bigFilter[0]));
            pushWindow("../images/5/bigLinuxColr.jpg", bigLinuxColr, bigLinuxColr.getSize());

            sf::Image lowPassLinux7 = applyFilter(imD, filter7);
            pushWindow("../images/5/lowPassLinux7.jpg", lowPassLinux7, lowPassLinux7.getSize());

            sf::Image gLinuxColor = gaussianNoise(imD, 0.15f);
            pushWindow("../images/5/gLinuxColor.jpg", gLinuxColor, gLinuxColor.getSize());

            sf::Image gLinuxGray = gaussianNoise(im1, 0.15f);
            pushWindow("../images/5/gLinuxGray.jpg", gLinuxGray, gLinuxGray.getSize());

            sf::Image archLinuxFilter7Gray = applyFilter(gLinuxGray, filter7);
            pushWindow("../images/5/archLinuxFilter7Gray.jpg", archLinuxFilter7Gray, archLinuxFilter7Gray.getSize());
            sf::Image archLinuxFilter7Color = applyFilter(gLinuxColor, filter7);
            pushWindow("../images/5/archLinuxFilter7Color.jpg", archLinuxFilter7Color, archLinuxFilter7Color.getSize());
        }
        else
        {
            pushWindow("../images/default.jpg", im0, im0.getSize());
            std::cout << "default image shown" << std::endl;
        }

        while (true) 
        {
            std::size_t open = 0u;
            
            for (std::size_t i = 0u; i < count; i++) 
            {
                if (windows[i].isOpen()) { open++; }
            }

            if (open == 0) {
                break;
            }

            for (std::size_t i = 0u; i < count; i++) {
                windows[i].run();
            }
        }

        for (std::size_t i = 0u; i < count; i++) 
        {
            windows[i].screenShot();
        }
    }

private:
    std::array<MoreWindows, 22> windows = {};
    std::size_t count = 0;
};

int main (int argc, char* argv[])
{
    menu();

    if (argc != 3)
    {
        std::cerr << "Usage: ./zestawnr <task> <operation/all>" << std::endl;
        return 1;
    }

    std::string task(argv[1]);
    std::string operation(argv[2]);
    Application app;
    app.run(task, operation);

    return 0;
}
