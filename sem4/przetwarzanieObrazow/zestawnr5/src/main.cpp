#include <functions.hpp>
#include <array>
#include <iostream>

void menu()
{
    const std::string tab = "        ";
    const std::string tabBig = "                     ";
}

class MoreWindows {
public:
    MoreWindows() = default;

    MoreWindows(
        const std::string & title, 
        const sf::Sprite & sprite, 
        const sf::Texture & texture, 
        sf::Vector2u size) 
        : savePath(title), sprite(sprite), texture(texture) 
    {
        this->sprite.setTexture(this->texture);
        window.create(sf::VideoMode(size.x, size.y), title, sf::Style::None);
        renderTexture.create(size.x, size.y);
    }

    void screenShot() const 
    {
        renderTexture.getTexture().copyToImage().saveToFile(savePath);
    }

    void run() {

        if (!window.isOpen()) { return; }

        sf::Event event;
        while (window.pollEvent(event)) 
        {
            if (event.type == sf::Event::Closed) { window.close(); }
        }

        renderTexture.clear(sf::Color::Black);
        renderTexture.draw(sprite);
        renderTexture.display();

        window.clear();
        sf::Sprite spr (renderTexture.getTexture());
        window.draw(spr);
        window.display();
    }

    bool isOpen() const {
        return window.isOpen();
    }

private:
    sf::Texture texture;
    const std::string savePath; 
    sf::RenderWindow window;
    sf::RenderTexture renderTexture;
    sf::Sprite sprite;
};

class Application 
{    
    void pushWindow(const std::string & title, const sf::Image & im, sf::Vector2u size) 
    {
        sf::Texture texture;
        texture.loadFromImage(im);
        sf::Sprite sprite;
        sprite.setTexture(texture);
    
        new (&windows[count++]) MoreWindows { title, sprite, texture, size };
    }

    void pushWindow(const std::string & title, const sf::Sprite & sprite, sf::Vector2u size) 
    {
        new (&windows[count++]) MoreWindows { title, sprite, *sprite.getTexture(), size };
    }
    
public:

    void run(const std::string & task, const std::string & operation)
    {
        sf::Image im0, im1;
        im0.loadFromFile("../../images/arch.jpg");
        im1.loadFromFile("../../images/kobieta1.jpg");
        
        int imgHeight = (im0.getSize().y);
        int imgWidth  = (im0.getSize().x);
        
        sf::Texture resultTexture;
        std::pair<int, int> tempSize = std::make_pair(imgWidth,imgHeight);

        /*
            upper filters
        */

        const float filter3[3][3] = { 
            { -1, -1, -1, },
            { -1,  9,  2, },
            { -1, -1, -1, },
        };

        const float filter5[5][5] = { 
            {  0, -1, -1, -1,  0 },
            { -1,  2, -4,  2, -1 },
            { -1, -4, 13, -4, -1 },
            { -1,  2, -4,  2, -1 },
            {  0, -1, -1, -1,  0 },
        };

        const float laplace[3][3] = { 
            {  0, -1,  0, },
            { -1,  4, -1, },
            {  0, -1,  0, },
        };

        const float prewitt[3][3] = { 
            { -1, -1, -1 },
            {  0,  0,  0 },
            {  1,  1,  1 },
        };

        const float sobel[3][3] = { 
            { -1, -2, -1 },
            {  0,  0,  0 },
            {  1,  2,  1 },
        };
        
        const float sobel90r[3][3] = { 
            {  1,  0, -1 },
            {  2,  0, -2 },
            {  1,  0, -1 },
        };

        const float emboss[3][3] = { 
            { -1, -1, -1 },
            {  0,  1,  0 },
            {  1,  1,  1 },
        };

        
        // bigger size ims
        sf::Image bIm3 = additionalBorders(im1, std::size(filter3[0]));
        sf::Image bIm5 = additionalBorders(im1, std::size(filter5[0]));
        
        // gray / 3/5 same size/big
        if(task == "1") 
        {
            sf::Image hiPas3Smal = applyFilter(im1, filter3);
            sf::Image hiPas5Smal = applyFilter(im1, filter5);
            pushWindow("../images/1/hiPas3Smal.jpg", hiPas3Smal, hiPas3Smal.getSize());
            pushWindow("../images/1/hiPas5Smal.jpg", hiPas5Smal, hiPas5Smal.getSize());
            
            sf::Image hiPas3Big = applyFilter(bIm3, filter3);
            sf::Image hiPas5Big = applyFilter(bIm5, filter5);
            pushWindow("../images/1/hiPas3Big.jpg", hiPas3Big, hiPas3Big.getSize());
            pushWindow("../images/1/hiPas5Big.jpg", hiPas5Big, hiPas5Big.getSize());
        }
        //  laplace same/Big
        else if(task == "2")
        {
            sf::Image laplaceSmal = applyFilter(im1, laplace);
            pushWindow("../images/2/laplaceSmal.jpg", laplaceSmal, laplaceSmal.getSize());
            
            sf::Image laplaceBig = applyFilter(bIm3, laplace);
            pushWindow("../images/2/laplaceBig.jpg", laplaceBig, laplaceBig.getSize());
        }
        // prewitt same/Big
        else if(task == "3")
        {
            sf::Image prewittSmal = applyFilter(im1, prewitt);
            pushWindow("../images/3/prewittSmal.jpg", prewittSmal, prewittSmal.getSize());
            
            sf::Image prewittBig = applyFilter(bIm3, prewitt);
            pushWindow("../images/3/prewittBig.jpg", prewittBig, prewittBig.getSize());
        }
        // sobel / rotate 0 / rotate 90 /  sobel / same/big
        else if(task == "4")
        {
            sf::Image sobelSmal = applyFilter(im1, sobel);
            sf::Image sobel90rSmal = applyFilter(im1, sobel90r);
            pushWindow("../images/4/sobelSmal.jpg", sobelSmal, sobelSmal.getSize());
            pushWindow("../images/4/sobel90rSmal.jpg", sobel90rSmal, sobel90rSmal.getSize());
            
            sf::Image sobelBig = applyFilter(bIm3, sobel);
            sf::Image sobel90rBig = applyFilter(bIm3, sobel90r);
            pushWindow("../images/4/sobelBig.jpg", sobelBig, sobelBig.getSize());
            pushWindow("../images/4/sobel90rBig.jpg", sobel90rBig, sobel90rBig.getSize());
        }
        // emboss / same/big
        else if(task == "5")
        {
            sf::Image embossSmal = applyFilter(im1, emboss);
            pushWindow("../images/5/embossSmal.jpg", embossSmal, embossSmal.getSize());
            
            sf::Image embossBig = applyFilter(bIm3, emboss);
            pushWindow("../images/5/embossBig.jpg", embossBig, embossBig.getSize());
        }
        // gray / line detection / 3/5 same/big
        else if(task == "6")
        {
            const float f1[3][3] = { 
                { -1, -1, -1 },
                {  2,  2,  2 },
                { -1, -1, -1 },
            };

            const float f2[3][3] = { 
                { 0,-1, 0 },
                { 0, 1, 0 },
                { 0, 0, 0 },
            };

            sf::Image line1smal = applyFilter(im1, f1);
            sf::Image line2smal = applyFilter(im1, f2);
            pushWindow("../images/6/line1smal.jpg", line1smal, line1smal.getSize());
            pushWindow("../images/6/line2smal.jpg", line2smal, line2smal.getSize());

            sf::Image line1big = applyFilter(bIm3, f1);
            sf::Image line2big = applyFilter(bIm3, f2);
            pushWindow("../images/6/line1big.jpg", line1big, line1big.getSize());
            pushWindow("../images/6/line2big.jpg", line2big, line2big.getSize());
        }
        else
        {   
            pushWindow("../images/default.jpg", im0, im0.getSize());
            std::cout << "default image shown" << std::endl;
        }

        while (true) 
        {
            std::size_t open = 0u;
            
            for (std::size_t i = 0u; i < count; i++) 
            {
                if (windows[i].isOpen()) { open++; }
            }

            if (open == 0) {
                break;
            }

            for (std::size_t i = 0u; i < count; i++) {
                windows[i].run();
            }
        }

        for (std::size_t i = 0u; i < count; i++) 
        {
            windows[i].screenShot();
        }
    }

private:
    std::array<MoreWindows, 10> windows = {};
    std::size_t count = 0;
};

int main (int argc, char* argv[])
{
    menu();

    if (argc != 3)
    {
        std::cerr << "Usage: ./zestawnr <task> <operation/all>" << std::endl;
        return 1;
    }

    std::string task(argv[1]);
    std::string operation(argv[2]);
    Application app;
    app.run(task, operation);

    return 0;
}
